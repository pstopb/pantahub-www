/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'

import { tokenKey } from '../../lib/auth'

const authInfo = window.localStorage.getItem(tokenKey)
const [token] = (authInfo || '').split('|')

const oauth2InitialState = {
  redirect_uri: '',
  oauth2Error: null,
  token: token || null,
  oauth2Token: null,
  loading: false,
  state: null
}

export const oauth2Reducer = (state = oauth2InitialState, action) => {
  switch (action.type) {
    case Types.OAUTH2_AUTHORIZE_INPROGR:
      return Object.assign({}, state, {
        loading: true,
        oauth2Error: null,
        token: token || null,
        oauth2Token: null,
        redirect_uri: ''
      })
    case Types.OAUTH2_AUTHORIZE_SUCCESS:
      return Object.assign({}, state, {
        redirect_uri: action.response.redirect_uri,
        oauth2Error: null,
        token: token || null,
        oauth2Token: action.response.token,
        loading: false
      })
    case Types.OAUTH2_AUTHORIZE_FAILURE:
      return Object.assign({}, state, {
        redirect_uri: '',
        token: token || null,
        oauth2Token: null,
        oauth2Error: action.error,
        loading: false
      })
    default:
      return state
  }
}
