/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'

import { authReducer as auth } from './auth/reducer'
import { devsReducer as devs } from './devices/reducer'
import { dashReducer as dash } from './dashboard/reducer'
import { navReducer as nav } from './nav/reducer'
import { oauth2Reducer as oauth2 } from './oauth2/reducer'
import { generalFlash as flash } from './general-flash/reducer'
import { reduceFunction as applications } from './applications/reducers'
import { reduce as claims } from './claims/reducer'
import { reduce as profile } from './profile/reducer'
import { reduce as images } from './images/reducer'
import { reduce as billing } from './billing/reducer'

const rootReducer = (history) => combineReducers({
  auth,
  devs,
  dash,
  nav,
  oauth2,
  flash,
  applications,
  claims,
  profile,
  images,
  billing,
  router: connectRouter(history)
})

export default rootReducer
