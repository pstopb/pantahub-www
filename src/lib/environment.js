const environment = {}

if (!window.env) {
  window.env = {}
}

Object.keys(process.env).forEach(key => {
  if (window.env[key] === undefined) {
    window.env[key] = process.env[key]
  }
  environment[key] = window.env[key]
})

export default environment
