/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { GetWwwURL, GetApiURL, GetEnv } from './const.helpers'

export const BASE_OAUTH_URL = () => `${GetApiURL()}/auth/oauth/login`
export const OAUTH_RETURN_TO_URL = () => `${GetWwwURL()}/login`

const baseService = {
  GetUrl () {
    return encodeURI(`${BASE_OAUTH_URL()}/${this.id}?redirect_uri=${OAUTH_RETURN_TO_URL()}`)
  }
}

export const OAUTH_SERVICES = [
  {
    id: 'google',
    name: 'Google',
    icon: 'mdi-google',
    isActive: () => GetEnv('REACT_APP_SOCIAL_OAUTH_GOOGLE_DISABLE', 'false') === 'false'
  },
  {
    id: 'github',
    name: 'Github',
    icon: 'mdi-github',
    isActive: () => GetEnv('REACT_APP_SOCIAL_OAUTH_GITLAB_DISABLE', 'false') === 'false'
  },
  {
    id: 'gitlab',
    name: 'Gitlab',
    icon: 'mdi-gitlab',
    isActive: () => GetEnv('REACT_APP_SOCIAL_OAUTH_GITHUB_DISABLE', 'false') === 'false'
  }
].map((s) => Object.assign(s, baseService))
