import ValidaJs from 'valida-js'
import get from 'lodash.get'

export const FileSize = (stateMap, type, name, compareWithValue = 0, defaultValue = '') => {
  const errorMessage = (current) =>
    `size of ${name} is ${current.toFixed(2)} should be lest than ${compareWithValue} Kb`

  return (state) => {
    const value = get(state, stateMap, defaultValue)

    if (!value || value === '') {
      return ValidaJs.factoryValidationObj(true, type, name, '')
    }

    if (value && value !== '' && (value.indexOf('http://') >= 0 || value.indexOf('https://') >= 0)) {
      return ValidaJs.factoryValidationObj(true, type, name, '')
    }

    const size = parseInt(value.replace(/=/g, '').length * 0.75)

    const isValid = size <= (compareWithValue * 1024)
    return ValidaJs.factoryValidationObj(isValid, type, name, errorMessage(size / 1024))
  }
}

export const ValidateIn = (stateMap, type, name, compareWithValue = [], defaultValue = '') => {
  return (state) => {
    const value = get(state, stateMap, defaultValue)
    const isValid = compareWithValue.some(c => c === value)
    return ValidaJs.factoryValidationObj(isValid, type, name, `${name} should be in [${compareWithValue.join(', ')}] and got ${value || 'empty'}`)
  }
}
