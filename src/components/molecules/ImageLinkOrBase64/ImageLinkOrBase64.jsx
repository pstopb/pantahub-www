/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState, useRef } from 'react'
import FileInput from '../../atoms/FileInput/FileInput'
import TextInput from '../../atoms/TextInput/TextInput'

export default function ImageLinkOrBase64 (props) {
  const {
    maximunSize = 15,
    ...rest
  } = props
  const fileUpload = useRef()
  const [state, setState] = useState({ error: null, dragOver: false })

  const reader = new window.FileReader()

  reader.onloadend = function () {
    if (reader.result.indexOf('data:image') === -1) {
      setState({
        ...state,
        error: 'You need to upload an image (jpg, png, gif, etc)',
        preview: null
      })

      props.onChange({ target: { value: '' } })

      return
    }
    setState({ ...state, error: null })
    props.onChange({ target: { value: reader.result } })
  }

  const onDragOver = (event) => {
    event.preventDefault()
  }

  const onDragleave = (event) => {
    event.preventDefault()
    setState({ ...state, dragOver: false })
  }

  const onDragStart = (event, id) => {
    event.preventDefault()
    setState({ ...state, dragOver: true })
  }

  const onDrop = (event) => {
    event.preventDefault()
    const dt = event.dataTransfer
    const files = dt.files

    const newEvent = new window.Event('change')
    fileUpload.current.files = files
    fileUpload.current.dispatchEvent(newEvent)
  }

  const openUploadDialog = (event) => {
    event.preventDefault()
    if (fileUpload.current) {
      fileUpload.current.click()
    }
  }

  const onFile = () => {
    const files = fileUpload.current.files
    if (files.length === 0) {
      setState({
        ...state,
        error: 'Please drop a file',
        dragOver: false,
        picture: null
      })

      return
    }

    if (files[0] && files[0].size > (maximunSize * 1024)) {
      const actualSize = files[0].size / 1024
      setState({
        ...state,
        error: `${props.name} image size is ${actualSize.toFixed(2)} Kb and should not be bigger than ${maximunSize} Kb`,
        dragOver: false,
        picture: null
      })

      return
    }

    setState({ ...state, dragOver: false })

    reader.readAsDataURL(files[0])
  }

  return (
    <React.Fragment>
      <TextInput
        {...rest}
        error={state.error}
      />
      <div className="invisible" style={{ height: 0 }}>
        <FileInput
          ref={fileUpload}
          onChange={onFile}
        />
      </div>
      <div
        draggable
        className={`drap-drop-area ${state.dragOver ? 'highlight' : ''}`}
        onDragOver={onDragOver}
        onDragStart={onDragStart}
        onDragEnter={onDragStart}
        onDragLeave={onDragleave}
        onDrop={onDrop}
        onClick={openUploadDialog}
      >
        {props.value && props.value !== ''
          ? (
            <img
              alt={`${props.name} preview`}
              src={props.value}
              style={{ maxWidth: '100%' }}
            />
          )
          : (
            <div>
            - or - <br/>
            Drag your logo image here
            </div>
          )}
        {state.error && (
          <p className="text-danger">
            {state.error}
          </p>
        )}
      </div>
    </React.Fragment>
  )
}
