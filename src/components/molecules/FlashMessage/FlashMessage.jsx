/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { ClearFlash } from '../../../store/general-flash/actions'
import { HTTP_STATUS } from '../../../lib/api.helpers'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

import './flash_message.scss'

function FlashMessage ({ type = 'error', message, dismissError }) {
  const errorClass = `general-errors general-errors--${type}`

  useEffect(() => {
    setTimeout(() => { dismissError() }, 10000)
  }, [dismissError])

  return (
    <div className={errorClass}>
      <div className="general-errors__wrapper">
        <span className="general-errors__message">
          {message}
        </span>
        <TrackedButton className="general-errors__close btn btn-link" onClick={dismissError}>
          <i className="mdi mdi-close-box"></i>
        </TrackedButton>
      </div>
    </div>
  )
}

function GeneralFlashMessage ({ flash, dismissError }) {
  if (flash.code === HTTP_STATUS.FORBIDDEN) {
    return (
      <FlashMessage
        code={flash.code}
        type="error"
        message="Your don't have permissions to this resource"
        dismissError={dismissError}
      />
    )
  }

  if (flash.code === 'general') {
    return (
      <FlashMessage
        code={flash.code}
        type={flash.type}
        message={flash.message}
        dismissError={dismissError}
      />
    )
  }

  return null
}

export default connect(
  state => ({ flash: state.flash }),
  dispatch => ({
    dismissError: (event) => {
      if (event && event.preventDefault) {
        event.preventDefault()
      }
      dispatch(ClearFlash())
    }
  })
)(GeneralFlashMessage)
