/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from 'react'
import { GetEnv } from '../../../lib/const.helpers'
import { OAUTH_SERVICES } from '../../../lib/oauth.helpers'
import OAuthLoginButton from '../../atoms/OAuthLoginButton/OAuthLoginButton'

import './oauthbuttons.scss'

export default function OAuthButtons ({ btnMessage, text, size = 'default' }) {
  // eslint-disable-next-line no-console
  if (GetEnv('REACT_APP_SOCIAL_OAUTH_DISABLE', 'false') === 'true') {
    return null
  }

  return (
    <section className="oauth-buttons">
      {text && (
        <header>
          <h6>{text}</h6>
        </header>
      )}
      <div className={`oauth-buttons--${size}`}>
        {OAUTH_SERVICES.map(service => service.isActive() && (
          <OAuthLoginButton
            key={service.name}
            message={btnMessage}
            size={size}
            service={service} />
        ))}
      </div>
    </section>
  )
}
