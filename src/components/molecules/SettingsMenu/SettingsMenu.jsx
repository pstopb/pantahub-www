/*
 * Copyright (c) 2017-2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as routes from '../../../router/routes'

function SettingsMenu (props) {
  const pageIs = (route) => {
    return props.location.pathname === route
  }

  const getStyle = (route) => {
    return {
      fontWeight: pageIs(route) ? 'bold' : 'normal',
      backgroundColor: pageIs(route) ? 'rgba(220, 220, 220, 0.3)' : 'white',
      borderRight: pageIs(route) ? '4px var(--bs-primary) solid' : 'none'
    }
  }
  return (
    <div className="btn-group btn-group-vertical border-radius-none" style={{ width: '100%' }}>
      <Link
        className="btn btn-light btn-group-lg"
        style={getStyle(routes.editProfile)}
        to={routes.editProfile}
      >
        Profile
      </Link>
      <Link
        className="btn btn-light btn-group-lg"
        style={getStyle(routes.globalConfig)}
        to={routes.globalConfig}
      >
        Settings
      </Link>
      <Link
        className="btn btn-light btn-group-lg"
        style={getStyle(routes.globalConfig)}
        to={routes.subscriptionsPath}
      >
        Subscriptions
      </Link>
      <Link
        className="btn btn-light btn-group-lg"
        style={getStyle(routes.globalConfig)}
        to={routes.payersPath}
      >
        Payers
      </Link>
    </div>
  )
}

export default connect(state => ({
  location: state.router.location
}))(SettingsMenu)
