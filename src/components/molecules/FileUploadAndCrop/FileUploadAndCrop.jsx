/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState, useCallback, useRef } from 'react'
import Cropper from 'react-easy-crop'
import GetCroppedImg from '../../../lib/images'
import FileInput from '../../atoms/FileInput/FileInput'
import './styles.scss'

export default function FileUploadAndCrop (props) {
  const {
    maximunSize = 15,
    maxHeight = 100,
    crop = { x: 0, y: 0 },
    zoom = 1,
    aspect = 1,
    onChange,
    value,
    style,
    message,
    pictureStyle,
    cropperStyle,
    ...rest
  } = props

  const fileUpload = useRef()
  const [state, setState] = useState({
    error: null,
    dragOver: false,
    cropping: false,
    picture: value
  })
  const [innerCrop, onCropChange] = React.useState(crop)
  const [innerZoom, onZoomChange] = React.useState(zoom)
  const [cropped, setCropped] = useState({
    area: undefined,
    pixels: undefined
  })

  const saveCroppedImage = useCallback(async () => {
    try {
      const croppedImage = await GetCroppedImg(
        state.picture,
        cropped.pixels
      )
      setState({
        error: null,
        cropping: false,
        picture: croppedImage
      })

      onChange({ target: { value: croppedImage } })
    } catch (e) {
      setState({
        ...state,
        error: e.message,
        cropping: true
      })
    }
  }, [state, cropped, onChange])

  const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
    setCropped({ area: croppedArea, pixels: croppedAreaPixels })
  }, [])

  const openUploadDialog = (event) => {
    event.preventDefault()
    if (fileUpload.current) {
      fileUpload.current.click()
    }
  }

  const reader = new window.FileReader()

  reader.onloadend = function () {
    if (reader.result.indexOf('data:image') === -1) {
      setState({
        ...state,
        error: 'You need to upload an image (jpg, png, gif, etc)',
        picture: undefined
      })
      return
    }
    setState({ ...state, error: null, picture: reader.result, cropping: true })
  }

  const onDragOver = (event) => {
    event.preventDefault()
  }

  const onDragleave = (event) => {
    event.preventDefault()
    setState({ ...state, dragOver: false })
  }

  const onDragStart = (event, id) => {
    event.preventDefault()
    setState({ ...state, dragOver: true })
  }

  const onFile = () => {
    const files = fileUpload.current.files
    if (files.length === 0) {
      setState({
        ...state,
        error: 'Please drop a file',
        dragOver: false,
        picture: null
      })

      return
    }

    if (files[0] && files[0].size > (maximunSize * 1024)) {
      const actualSize = files[0].size / 1024
      setState({
        ...state,
        error: `${props.name} image size is ${actualSize.toFixed(2)} Kb and should not be bigger than ${maximunSize} Kb`,
        dragOver: false,
        picture: null
      })

      return
    }

    setState({ ...state, dragOver: false })

    reader.readAsDataURL(files[0])
  }

  const onDrop = (event) => {
    event.preventDefault()
    const dt = event.dataTransfer
    const files = dt.files
    const newEvent = new window.Event('change')
    fileUpload.current.files = files
    fileUpload.current.dispatchEvent(newEvent)
  }

  return (
    <React.Fragment>
      <FileInput
        {...rest}
        ref={fileUpload}
        error={state.error}
        onChange={onFile}
        className="invisible"
        style={{ height: 0 }}
      />
      {!state.cropping && (
        <React.Fragment>
          <div
            draggable
            style={{ minHeight: maxHeight, ...style }}
            className={`drap-drop-area ${state.dragOver ? 'highlight' : ''}`}
            onDragOver={onDragOver}
            onDragStart={onDragStart}
            onDragEnter={onDragStart}
            onDragLeave={onDragleave}
            onDrop={onDrop}
          >
            {state.picture && state.picture !== '' && (
              <img
                onClick={openUploadDialog}
                alt={`${props.name} preview`}
                src={state.picture}
                style={{ maxWidth: '100%' }}
              />
            )}
            {!state.picture && (
              <div
                onClick={openUploadDialog}
                style={pictureStyle}
              >
                {message || 'Drag your logo image here'}
              </div>
            )}
            {state.error && (
              <p className="text-danger">
                {state.error}
              </p>
            )}
          </div>
        </React.Fragment>
      )}
      {state.picture && state.picture !== '' && state.cropping && (
        <React.Fragment>
          <div
            className="cropper-wrapper"
            style={{
              ...cropperStyle,
              position: 'relative',
              height: maxHeight
            }}
          >
            <Cropper
              image={state.picture}
              crop={innerCrop}
              zoom={innerZoom}
              aspect={aspect}
              height={maxHeight}
              unit="px"
              onCropComplete={onCropComplete}
              onCropChange={onCropChange}
              onZoomChange={onZoomChange}
              onMediaLoaded={mediaSize => {
                // Adapt zoom based on media size to fit max height
                // onZoomChange(maxHeight / mediaSize.naturalHeight)
              }}
            />
          </div>
          <div className="d-flex justify-content-center pt-4 mb-4">
            <a
              href="#"
              className="btn btn-primary"
              onClick={saveCroppedImage}
            >
              Crop this area
            </a>
          </div>
        </React.Fragment>
      )}
    </React.Fragment>
  )
}
