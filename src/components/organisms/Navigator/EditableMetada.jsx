/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'

import { deviceSetMeta } from '../../../store/devices/actions'
import { capitalize } from '../../../lib/utils'
import { EditableMeta } from '../../molecules/EditableMeta/EditableMeta'
import { getProfileMetaData } from '../../../services/profiles.service'

class UserDeviceNavigatorMetadata extends Component {
  state = {
    editedMeta: null,
    editing: false,
    newKey: '',
    newValue: '',
    globalMeta: {}
  }

  async componentDidMount () {
    this.setState({ loading: true })
    try {
      const response = await getProfileMetaData(this.props.token)
      if (!response.ok) {
        this.setState({ error: response.json, loading: false })
      }
      this.setState({ globalMeta: response.json, loading: false })
    } catch (e) {
      this.setState({ error: e, loading: false })
    }
  }

  title (type) {
    return type ? capitalize(type.replace('-', ' ')) : undefined
  }

  meta = () => {
    const { device, type = 'user-meta' } = this.props
    const usermeta = device[type] || {}

    return Object.keys(usermeta).reduce((acc, key) => {
      let type = this.state.globalMeta[key] ? 'global' : 'user'
      if (usermeta[key] && usermeta[key] !== this.state.globalMeta[key]) {
        type = 'user'
      }
      acc[key] = {
        type: type,
        value: usermeta[key]
      }
      return acc
    }, {})
  }

  saveHandler = async (oldKey, newKey, oldValue, newValue) => {
    const fullMeta = {
      ...this.meta(),
      ...(this.state.editedMeta || {})
    }
    delete fullMeta[oldKey]
    this.setState({
      editedMeta: { ...fullMeta },
      editing: false
    })
    let type = this.state.globalMeta[newKey] ? 'global' : 'user'
    if (this.state.editedMeta && this.state.editedMeta[newKey] && this.state.editedMeta[newKey] !== this.state.globalMeta[newKey]) {
      type = 'user'
    }
    fullMeta[newKey] = { type, value: newValue }
    await this.props.deviceSetMeta(
      this.props.token,
      this.props.device.id,
      this.getApiMeta(fullMeta),
      this.props.type
    )
    this.setState({
      editedMeta: fullMeta
    })
  }

  editHandler = edit => {
    this.setState({
      editing: edit
    })
  }

  deleteHandler = (key) => {
    const fullMeta = {
      ...this.meta(),
      ...(this.state.editedMeta || {})
    }
    if (this.state.globalMeta[key]) {
      fullMeta[key] = { type: 'global', value: null }
    } else {
      delete fullMeta[key]
    }
    this.setState({
      editedMeta: fullMeta
    })
    this.props.deviceSetMeta(
      this.props.token,
      this.props.device.id,
      this.getApiMeta(fullMeta),
      this.props.type
    )
  }

  addHandler = () => {
    const fullMeta = {
      ...this.meta(),
      ...(this.state.editedMeta || {})
    }
    const editedMeta = {
      ...fullMeta,
      [this.state.newKey]: { type: 'user', value: this.state.newValue }
    }
    this.setState({
      newKey: '',
      newValue: '',
      editedMeta: editedMeta
    })

    if (this.state.newKey && this.state.newValue) {
      this.props.deviceSetMeta(
        this.props.token,
        this.props.device.id,
        this.getApiMeta(editedMeta),
        this.props.type
      )
    }
  }

  getApiMeta = (meta) => {
    return Object.keys(meta).reduce((acc, key) => {
      acc[key] = meta[key].value

      return acc
    }, {})
  }

  newValueHandler = evt => this.setState({ newValue: evt.target.value })
  newKeyHandler = evt => this.setState({ newKey: evt.target.value })

  render () {
    const { navigator, type } = this.props
    const { newKey, newValue, editing } = this.state
    const loading = navigator.setMeta.loading

    return (
      <EditableMeta
        title={this.title(type)}
        meta={this.meta()}
        globalMeta={this.state.globalMeta}
        editedMeta={this.state.editedMeta || this.meta()}
        loading={loading}
        editing={editing}
        newKey={newKey}
        newValue={newValue}
        saveHandler={this.saveHandler}
        editHandler={this.editHandler}
        deleteHandler={this.deleteHandler}
        addHandler={this.addHandler}
        newKeyHandler={this.newKeyHandler}
        newValueHandler={this.newValueHandler}
      />
    )
  }
}

export default connect(null, { deviceSetMeta })(UserDeviceNavigatorMetadata)
