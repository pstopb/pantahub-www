/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import sortBy from 'lodash.sortby'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import TruncatedText from '../../atoms/TruncatedText/TruncatedText'
import { userDashboardPath } from '../../../router/routes'
import { classForDevStatus } from '../../../lib/utils'
import { devicePostRev } from '../../../store/devices/actions'
import { STATES } from '../../../store/devices/reducer'
import { TrackedButton } from '../../atoms/Tracker/Tracker'
import { ReduceSignatures } from '../../../services/devices.service'
import { SignedIcon } from '../../atoms/SignedIcon/SignedIcon'

class UserDeviceNavigatorHistory extends Component {
  render () {
    const { device, username, loadingState } = this.props
    const deployStep = (step) => () => {
      const payload = {
        state: step.state,
        meta: step.meta,
        rev: -1,
        'commit-msg': `Redeploy ${step['commit-msg']}`
      }
      return this.props.devicePostRev(device.id, payload)
    }

    return (
      <div className="table-responsive">
        <table className="table table-borderless table-striped table-responsive table-no-first-border">
          <thead>
            <tr>
              <th />
              <th>Commit ID (Rev)</th>
              <th>Timestamp</th>
              <th>Status</th>
              <th>Commit Message</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {sortBy((device.history || {}).steps || [], [s => -1 * s.rev]).map(
              (s) => {
                const current =
                  s.rev === ((device.history || {}).currentStep || {}).rev
                // const deployed = s.rev === array[0].rev
                const status = (s.progress || {}).status || ''
                const signatures = Object.entries(s.state).reduce(ReduceSignatures, { include: [], exclude: [] })
                const timestamp = new Date(s['step-time']).toLocaleString()
                return (
                  <tr
                    key={s.rev}
                    style={{ verticalAlign: 'middle' }}
                    className={current ? 'bg-light font-weight-bold' : ''}
                  >
                    <td>
                      {signatures.include.length > 0 && (
                        <Link
                          to={`${userDashboardPath}/${username}/devices/${device.id}/step/${s.rev}/files`}
                          className="btn btn-sm btn-default"
                          title="View revision details"
                        >
                          <SignedIcon />
                        </Link>
                      )}
                    </td>
                    <td>
                      <TruncatedText text={s['state-sha']} size={8} appendText=" " />
                      ({s.rev}){current ? '*' : ''}
                    </td>
                    <td>{timestamp}</td>
                    <td>
                      <span
                        className={`badge badge-${classForDevStatus(status)}`}
                      >
                        {status}
                      </span>
                    </td>
                    <td style={{ maxWidth: 'calc(100vw - 60vw)' }}>
                      <pre style={{ marginBottom: 0 }}>{s['commit-msg'] || ''}</pre>
                    </td>
                    <td>
                      <div className="btn-group" role="group" aria-label="Actions">
                        <TrackedButton
                          className="btn btn-sm btn-outline-dark"
                          disabled={loadingState === STATES.IN_PROGRESS}
                          onClick={deployStep(s)}
                          title="Deploy this revision"
                        >
                          <i className="mdi mdi-rocket mdi-rotate-315" aria-hidden="true" />
                        </TrackedButton>
                        <Link
                          to={`${userDashboardPath}/${username}/devices/${
                            device.id
                          }/step/${s.rev}`}
                          className="btn btn-sm btn-outline-dark"
                          title="View revision details"
                        >
                          <i className="mdi mdi-chevron-right" aria-hidden="true" />
                        </Link>
                      </div>
                    </td>
                  </tr>
                )
              }
            )}
          </tbody>
        </table>
      </div>
    )
  }
}

export default connect(
  (state, ownProps) => ({
    loadingState: state.devs.deploy[ownProps.device.id]
  }),
  { devicePostRev }
)(UserDeviceNavigatorHistory)
