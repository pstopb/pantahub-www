/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'

import './how_to_start.scss'
import { connect } from 'react-redux'
import { getImages } from '../../../store/images/actions'
import { GetEnv } from '../../../lib/const.helpers'
import DownloadImager from '../../molecules/DownloadImager/DownloadImager'
import DownloadImageSelector from '../../molecules/DownloadImageSelector/DownloadImageSelector'

import flashGif from '../../../assets/images/flash.gif'
import { Link } from 'react-router-dom'
import { userDashboardPath } from '../../../router/routes'

class HowToStartInner extends React.Component {
  componentDidMount () {
    if (this.props.images.items.length === 0) {
      this.props.getImages(GetEnv('REACT_APP_IMAGES_CI_CHANNEL'))
    }
  }

  render () {
    const { className, auth } = this.props
    const { username } = auth
    return (
      <div className={`hts getting-started ${className || ''}`}>
        <div className="pt-6 pb-6 hero bg-color blue">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-6 text-center">
                <h1 className="pt-4">
                  Let&apos;s start connecting devices
                </h1>
                <p className="pt-5 pb-5">
                  {`Just came here for the first time and wonder how to get started?
                  Head over to `}
                  <a href="https://docs.pantahub.com">
                    our docs
                  </a>
                  {` and follow one of the "Getting Started" guides to get going quickly.
                  Let us know about what you have done and how things could be better.`}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="bg-color bg-brown-light">
          <div className="container pb-5 pt-6">
            <div className="row hts__steps pb-4 justify-content-center">
              <div className="col-md-6 pb-4">
                <h1 className="text-center">Flash your first device</h1>
              </div>
            </div>
            <div className="row justify-content-center">
              <div className="col-md-8">
                <div className="row">
                  <div className="col-md-12 mt-4 mb-4">
                    <div className="row">
                      <div className="col-md-12 mb-4">
                        <div className="row align-items-center">
                          <div className="col-2">
                            <div className="hts__step-circle hts__step-circle--xs">
                              <span>1</span>
                            </div>
                          </div>
                          <div className="col-10">
                            <h2 className="font-weight-light mt-2">Download Pantavisor Image</h2>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-10 offset-md-2">
                        <div className="pt-1 pb-4">
                          <DownloadImageSelector images={this.props.images.items} />
                        </div>
                        <p>
                          Download your automaticly claimed pantavisor image and forget about claiming your device.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-12 mt-4 mb-4">
                    <div className="row">
                      <div className="col-md-12 mb-4">
                        <div className="row align-items-center">
                          <div className="col-2">
                            <div className="hts__step-circle hts__step-circle--xs">
                              <span>2</span>
                            </div>
                          </div>
                          <div className="col-10">
                            <h2 className="font-weight-light mt-2">
                              Download and install Raspberry Pi Imager
                            </h2>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-10 offset-md-2">
                        <p>
                          Raspberry Pi Imager is used to write the OS image you downloaded in Step 1 to your SD card.
                        </p>
                        <div className="pt-1 pb-1">
                          <DownloadImager />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-12 mt-4 mb-4">
                    <div className="row">
                      <div className="col-md-12 mb-4">
                        <div className="row align-items-center">
                          <div className="col-2">
                            <div className="hts__step-circle hts__step-circle--xs">
                              <span>3</span>
                            </div>
                          </div>
                          <div className="col-10">
                            <h2 className="font-weight-light mt-2">Flash your SD with Pantavisor</h2>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-10 offset-md-2">
                        <p>
                          Launch Raspberry Pi Imager, choose the file you downloaded in Step 1,
                          select your SD card or USB drive and click &quot;Flash&quot;.
                          This will wipe all data on the card and prepare it for your device.
                        </p>
                        <p>
                          <img src={flashGif} alt="how to flash"/>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-12 mt-4 mb-4">
                    <div className="row">
                      <div className="col-md-12 mb-4">
                        <div className="row align-items-center">
                          <div className="col-2">
                            <div className="hts__step-circle hts__step-circle--xs">
                              <span>4</span>
                            </div>
                          </div>
                          <div className="col-10">
                            <h2 className="font-weight-light mt-2">Start using PantacorHub</h2>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-10 offset-md-2">
                        <p>
                          Remove the SD card from your computer and put the card into the Raspberry Pi
                          Plug your Raspberry Pi to wired Ethernet and a strong enough power source
                        </p>
                        <video
                          autoPlay
                          loop
                          muted
                          width="700">
                          <source
                            src="https://one.pantacor.com/app/videos/rpiconnect.webm"
                            type="video/webm"
                          />
                          <source
                            src="https://one.pantacor.com/app/videos/rpiconnect.m4v"
                            type="video/mp4"
                          />
                          {'Sorry, your browser doesn\'t support embedded videos.'}
                        </video>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-12 mt-4 mb-4">
                    <p className="text-center">
                      <Link
                        className="btn  btn-primary"
                        to={`${userDashboardPath}/${username}/devices`}
                      >
                        See my new device in action
                      </Link>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  (state) => ({ auth: state.auth, images: state.images }),
  { getImages }
)(HowToStartInner)
