/*
 * Copyright (c) 2021 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState, memo } from 'react'
import { TrackedButton } from '../../atoms/Tracker/Tracker'
import { resolvePath } from '../../../lib/object.helpers'

export default memo(function UserDeviceNavigatorMetadataRow (props) {
  const [state, setState] = useState({
    inEdit: false,
    newKey: props.currentKey,
    newValue: props.value || ''
  })
  const {
    inProgress,
    loading,
    disabled,
    currentKey: key,
    onEdit,
    saveHandler,
    onDelete,
    type,
    globalMeta = {}
  } = props
  const { inEdit, newKey, newValue } = state
  const value = resolvePath(props, 'value.value', resolvePath(props, 'value'))

  const updateKey = evt => {
    setState({
      ...state, newKey: evt.target.value
    })
  }

  const updateValue = evt => {
    setState({
      ...state, newValue: evt.target.value
    })
  }
  return (
    <tr
      className={`${
        inProgress || (disabled && !inEdit) ? 'table-secondary' : ''
      } ${inEdit ? 'metadata-edit new-entry-form' : ''}`}
      title={`${inProgress ? 'Operation in progress' : ''}`}
    >
      {type && (
        <td>
          {type}
        </td>
      )}
      <td className="meta-key">
        {inEdit
          ? (
            <input
              className="form-control my-1 mr-sm-2"
              type="text"
              placeholder={key}
              value={newKey}
              onChange={updateKey}
            />
          )
          : (
            <pre
              style={{ textDecoration: globalMeta[key] && value === null ? 'line-through' : 'none' }}
            >{`${key}`}</pre>
          )}
      </td>
      <td>
        {inEdit
          ? (
            <textarea
              className="form-control my-1 mr-sm-2 textarea"
              aria-label="Metadata Field Value"
              placeholder={value}
              value={newValue}
              rows={Math.max(newValue.split('\n').length, 1)}
              onChange={updateValue}
            />
          )
          : (
            <pre
              style={{ textDecoration: globalMeta[key] && value === null ? 'line-through' : 'none' }}
            >{`${globalMeta[key] && value === null ? globalMeta[key] : value}`}</pre>
          )}
      </td>
      <td className="actions" width="150">
        <div className="btn-group my-1 mr-sm-2">
          {inEdit
            ? (
              <React.Fragment>
                <TrackedButton
                  style={{ color: 'white' }}
                  className={`btn btn-sm btn-primary ${
                    inProgress || loading ? 'disabled' : ''
                  }`}
                  disabled={inProgress || loading}
                  title="Save"
                  onClick={() => {
                    saveHandler(key, newKey, value, newValue, type)
                    setState({
                      ...state,
                      inEdit: false
                    })
                  }}
                >
                  <span
                    className={`mdi ${
                      inProgress ? 'mdi-sync-alert' : 'mdi-content-save'
                    }`}
                  />
                </TrackedButton>
                <TrackedButton
                  href={undefined}
                  style={{ color: 'white' }}
                  className={`btn btn-sm btn-info ${
                    inProgress || loading ? 'disabled' : ''
                  }`}
                  disabled={inProgress || loading}
                  title="Cancel"
                  onClick={() => {
                    setState({
                      ...state,
                      inEdit: false,
                      newKey: props.key,
                      newValue: props.value
                    })
                    onEdit(false)
                  }}
                >
                  <span
                    className={`mdi ${
                      inProgress ? 'mdi-sync-alert' : 'mdi-cancel'
                    }`}
                  />
                </TrackedButton>
              </React.Fragment>
            )
            : (
              <React.Fragment>
                {globalMeta[key] && globalMeta[key] !== value && (
                  <TrackedButton
                    style={{ color: 'white' }}
                    className={`btn btn-sm btn-info ${
                      inProgress || loading || disabled ? 'disabled' : ''
                    }`}
                    disabled={inProgress || loading || disabled}
                    title="Restor to global value"
                    onClick={() => {
                      setState({
                        ...state,
                        newValue: globalMeta[key],
                        newKey: key,
                        inEdit: true
                      })
                      onEdit(true)
                    }}
                  >
                    <span
                      className={`mdi ${
                        inProgress ? 'mdi-sync-alert' : 'mdi-backup-restore'
                      }`}
                    />
                  </TrackedButton>
                )}
                <TrackedButton
                  style={{ color: 'white' }}
                  className={`btn btn-sm btn-info ${
                    inProgress || loading || disabled ? 'disabled' : ''
                  }`}
                  disabled={inProgress || loading || disabled}
                  title="Edit this key"
                  onClick={() => {
                    setState({
                      ...state,
                      inEdit: true
                    })
                    onEdit(true)
                  }}
                >
                  <span
                    className={`mdi ${
                      inProgress ? 'mdi-sync-alert' : 'mdi-pencil'
                    }`}
                  />
                </TrackedButton>
              </React.Fragment>
            )}
          <TrackedButton
            style={{ color: 'white' }}
            className={`btn btn-sm btn-danger ${
              inProgress || loading || disabled ? 'disabled' : ''
            }`}
            disabled={inProgress || loading || disabled}
            title="Remove this key from metadata"
            onClick={() => {
              onDelete(key, type)
            }}
          >
            <span
              className={`mdi ${
                inProgress ? 'mdi-sync-alert' : 'mdi-delete'
              }`}
            />
          </TrackedButton>
        </div>
      </td>
    </tr>
  )
})
