/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Link } from 'react-router-dom'

import SignUpForm from '../../molecules/SignUpForm/SignUpForm'

import { logout } from '../../../store/auth/actions'

import { userDashboardPath, loginPath } from '../../../router/routes'
import { withAuthenticatedEvent } from '../../../store/ga-middleware'

import './landing.scss'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

class LandingPage extends Component {
  render () {
    const { token, username, onLogout } = this.props
    return (
      <div className="landing-page">
        <div className="row hero bg-color">
          <div className="container pt-8 pb-6">
            <div className="row justify-content-center">
              <div className="col-md-9 center-box">
                <div className="row d-flex justify-content-between">
                  <div className="col-md-6 welcome">
                    <div className="">
                      <header>
                        <h1 className="text-center">Welcome to PantacorHub</h1>
                      </header>
                      <p className="text-center">
                        The only place where Linux firmware can be <strong>shared and
                  deployed</strong> for any device.
                      </p>
                    </div>
                    <div className="row welcome-images justify-content-around">
                      <div className="welcome-images-item">
                        <div
                          className="welcome-image"
                          style={{
                            backgroundImage: 'url(/images/icon_connect.png)'
                          }}
                        />
                        <div className="welcome-image-text">
                          Connect your Devices
                        </div>
                      </div>
                      <div className="welcome-images-item">
                        <div
                          className="welcome-image"
                          style={{
                            backgroundImage: 'url(/images/icon_deploy.png)'
                          }}
                        />
                        <div className="welcome-image-text">
                          Deploy
                          <br/>
                          Software
                        </div>
                      </div>
                      <div className="welcome-images-item">
                        <div
                          className="welcome-image"
                          style={{
                            backgroundImage: 'url(/images/icon_share.png)'
                          }}
                        />
                        <div className="welcome-image-text">Share Firmwares</div>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-5 signup">
                    <div className="row justify-content-md-center pt-3 pb-5">
                      <div className="col-md-12">
                        {token
                          ? (
                            <React.Fragment>
                              <h3>Start using PantacorHub</h3>
                              <Link
                                to={`${userDashboardPath}/${username}`}
                                className="btn btn-primary btn-block"
                              >
                                Dashboard
                              </Link>
                              <Link
                                to={`${userDashboardPath}/${username}/devices`}
                                className="btn btn-primary btn-block"
                              >
                                Your Devices
                              </Link>
                              <a href="#contact_us" className="btn btn-primary btn-block">
                                Contact Us
                              </a>
                              <TrackedButton onClick={onLogout} className="btn btn-primary btn-block">
                                Log Out
                              </TrackedButton>
                            </React.Fragment>)
                          : (
                            <React.Fragment>
                              <h3 className="text-center">Register for Free Now</h3>
                              <SignUpForm labels={false} size="default" />
                              <p className="mt-3">
                                Already in PantacorHub? <Link to={loginPath}>Log In</Link>
                              </p>
                            </React.Fragment>
                          )
                        }
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row sell text-center bg-color pt-6">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-9 center-block">
                <div className="row justify-content-center">
                  <header className="col-md-12">
                    <h1 className="mb-5">A Hub for all your Linux devices</h1>
                  </header>
                  <div className="col-md-12">
                    <p>
                      Thanks to our Pantavisor device agent and through the use of container technology, PantacorHub lets you connect your devices and transform them into bare-metal infrastructure. Designed for deeply embedded and high-grade enterprise systems alike, Pantavisor and PantacorHub are the perfect choice for the management of your Linux devices and their firmware lifecycles.
                    </p>
                    <p>
                      Designed for deeply embedded and high-grade enterprise systems alike, Pantavisor and PantacorHub are the perfect choice for the management of your Linux devices and their firmware lifecycles.
                    </p>
                  </div>
                  <div className="col-10">
                    <img
                      style={{ maxWidth: '100%', marginTop: 90 }}
                      src="/images/products_drawing_hub.svg"
                      alt="products"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row pb-6 pt-6 features bg-color">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-9">
                <div className="row pt-5">
                  <div className="col-md-4 col-12">
                    <div className="feature text-center justify-content-center">
                      <div className="feature-image-row">
                        <div className="row justify-content-center">
                          <div
                            style={{ backgroundImage: 'url(/images/icon_container.png)' }}
                            className="feature-image"
                          />
                        </div>
                      </div>
                      <div className="feature-title-row mb-4">
                        <h5>Containers technology</h5>
                      </div>
                      <div className="feature-description-row">
                        <p>
                          We use container technology to make the development, deployment and management of firmware on Linux devices as simple as it has ever been. From tiny OpenWRT routers to carrier-grade base stations, PantacorHub and Pantavisor are the right choice.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-12">
                    <div className="feature text-center justify-content-center">
                      <div className="feature-image-row">
                        <div className="row justify-content-center">
                          <div
                            style={{ backgroundImage: 'url(/images/icon_collaborate.png)' }}
                            className="feature-image"
                          />
                        </div>
                      </div>
                      <div className="feature-title-row mb-4">
                        <h5>Share and Collaborate</h5>
                      </div>
                      <div className="feature-description-row">
                        <p>
                          Share your firmware and projects as simple as with a git tree:<br/>
                          <code>
                            pvr clone; pvr commit; pvr post
                          </code>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4 col-12">
                    <div className="feature text-center justify-content-center">
                      <div className="feature-image-row">
                        <div className="row justify-content-center">
                          <div
                            style={{ backgroundImage: 'url(/images/icon_fully_open.png)' }}
                            className="feature-image"
                          />
                        </div>
                      </div>
                      <div className="feature-title-row mb-4">
                        <h5>Fully Open</h5>
                      </div>
                      <div className="feature-description-row">
                        <p>
                          Since day one Pantacor has been committed to a fully open source model. Feel free to use and improve our technology! We welcome all contributions and hope that together we can build the future of connected Linux devices.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row contact text-center bg-color" id="contact_us">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-9">
                <div className="row">
                  <header className="col-md-12">
                    <h1>Contact Us</h1>
                  </header>
                  <div className="col-md-12">
                    <div className="row d-flex justify-content-center">
                      <div className="col-xs-8 col-md-6 pt-5 mb-2">
                        <p>
                          Questions, feedback or want to explore how our technology can help?
                          We love to hear from you, so just shoot us a mail.<br/><br/>
                          <a href="mailto:team@pantahub.com">team@pantahub.com</a>
                        </p>
                      </div>
                      <div className="col-xs-8 col-md-6 pt-5 mb-2">
                        <p>
                          For Entreprise and Sales questions or to find out
                          how to use PantacorHub in your device deployment
                          please contact sales at:<br/><br/>
                          <a href="mailto:sales@pantahub.com">sales@pantahub.com</a>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect(
  state => state.auth,
  dispatch => ({
    onLogout: () => dispatch(logout())
  })
)(withAuthenticatedEvent(LandingPage, (isLoggedIn) => (isLoggedIn ? 'Logged User Home' : 'Anonimous Home')))
