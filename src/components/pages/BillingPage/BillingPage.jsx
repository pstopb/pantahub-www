import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { Route, Switch } from 'react-router'
import { LoadStripePromise } from '../../../lib/const.helpers'
import {
  subscriptionChargesPath,
  orderCompletePath,
  paymentDetailsPath,
  paymentConfirmPath,
  subscriptionsNewPath,
  subscriptionsPath
} from '../../../router/routes'
import {
  GetCustomersAction,
  GetPricesAction,
  GetSubscriptionsAction
} from '../../../store/billing/actions'
import { SubscriptionsCharges } from '../../molecules/SubscriptionsCharges/SubscriptionsCharges'
import { ConfirmSubscription } from '../../organisms/ConfirmSubscription/ConfirmSubscription'
import { OrderComplete } from '../../organisms/OrderComplete/OrderComplete'
import { PaymentDetail } from '../../organisms/PaymentDetail/PaymentDetail'
import { SubscriptionCreate } from '../../organisms/SubscriptionCreate/SubscriptionCreate'
import { SubscriptionStatus } from '../../organisms/SubscriptionStatus/SubscriptionStatus'

LoadStripePromise()

export default function BillingPage () {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(GetCustomersAction())
    dispatch(GetSubscriptionsAction())
    dispatch(GetPricesAction())
  }, [])

  return (
    <Switch>
      <Route
        exact
        path={subscriptionsPath}
      >
        <SubscriptionStatus />
      </Route>
      <Route
        exact
        path={`${subscriptionsNewPath}/:subscription_id`}
      >
        <SubscriptionCreate />
      </Route>
      <Route
        exact
        path={orderCompletePath}
      >
        <OrderComplete />
      </Route>
      <Route
        exact
        path={paymentDetailsPath}
      >
        <PaymentDetail />
      </Route>
      <Route
        exact
        path={paymentConfirmPath}
      >
        <ConfirmSubscription />
      </Route>
      <Route
        exact
        path={subscriptionChargesPath}
      >
        <SubscriptionsCharges />
      </Route>
    </Switch>
  )
}
