/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import { getApp, updateApp, getScopes } from '../../../store/applications/actions'
import { userDashboardPath } from '../../../router/routes'
import AppForm from '../../organisms/AppForm/AppForm'

class Application extends Component {
  async componentDidMount () {
    if (this.props.match.params.id !== 'new') {
      await this.props.getApp(this.props.match.params.id)
    }
    this.props.getScopes()
  }

  render () {
    return (
      <div className="thridpartyapp-detail">
        <div
          key="breadcrumbs"
          className="breadcrumb-sector row align-items-center"
        >
          <div className="col-8">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <Link to={`${userDashboardPath}/user`}>Dashboard</Link>
              </li>
              <li className="breadcrumb-item">
                <Link to={`${userDashboardPath}/user/apps/`}>Applications</Link>
              </li>
              <li className="breadcrumb-item active">{this.props.match.params.id !== 'new' ? 'Edit app' : 'Create app'}</li>
            </ol>
          </div>
        </div>
        <AppForm updateApp={this.props.updateApp} {...this.props.applications} />
      </div>
    )
  }
}

export default connect(
  state => ({
    applications: state.applications
  }),
  {
    getApp,
    getScopes,
    updateApp
  }
)(Application)
