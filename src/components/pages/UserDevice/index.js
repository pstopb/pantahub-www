/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import Loading from '../../atoms/Loading/Loading'
import UserDeviceHeader from './Header'
// import UserDeviceInfo from './Info'

import { resolvePath } from '../../../lib/object.helpers'
import {
  initializeDevice,
  deviceNavigatorSetTab,
  deviceSetLogsFilter,
  deviceSetEditing
} from '../../../store/devices/actions'

import './user_device.scss'
import DevicesBreadcumbs from '../../atoms/DevicesBreadcumbs/DevicesBreadcumbs'
import { GetEnv } from '../../../lib/const.helpers'
import DeviceSubRoute from '../../organisms/DeviceSubRoute/DeviceSubRoute'

class UserDeviceInner extends Component {
  shouldComponentUpdate (nextProps) {
    if (nextProps.loading) {
      return true
    }

    const nextActiveTab = resolvePath(nextProps, 'navigator.activeTab')
    const currentActiveTab = resolvePath(this.props, 'navigator.activeTab')

    if (nextActiveTab !== currentActiveTab) {
      return true
    }

    const rev = resolvePath(nextProps, 'device.history.currentStep.rev')
    const stateDevice = resolvePath(nextProps, 'device.id')
    const nextRevision = parseInt(nextProps.match.params.revision, 10)
    const currentRevision = parseInt(this.props.match.params.revision, 10)
    const currentDevice = this.props.match.params.deviceId
    const nextDevice = nextProps.match.params.deviceId
    const sameRevision = !isNaN(nextRevision) && !isNaN(currentRevision) && nextRevision === currentRevision
    const differentDevice = currentDevice === nextDevice && stateDevice !== nextDevice
    const differentRevision = sameRevision && !isNaN(currentRevision) && rev && rev !== currentRevision
    if (differentRevision || differentDevice) {
      return false
    }
    return true
  }

  render () {
    const {
      device,
      deviceId,
      onChangeNavigatorTab,
      navigator,
      onSetEditing,
      onSetLogsFilter,
      activeTab,
      username,
      token,
      disabled,
      saving
    } = this.props

    return (
      <React.Fragment>
        <UserDeviceHeader
          username={username}
          device={device}
          deviceId={deviceId}
          disabled={disabled}
          editigHandler={onSetEditing}
          saving={saving}
          token={token}
        />
        <DeviceSubRoute
          onChangeNavigatorTab={onChangeNavigatorTab}
          navigator={navigator}
          onSetLogsFilter={onSetLogsFilter}
          activeTab={activeTab}
          device={device}
          deviceId={deviceId}
          username={username}
          token={token}
          disabled={disabled}
        />
      </React.Fragment>
    )
  }
}

class UserDevice extends Component {
  constructor (props) {
    super(props)
    this.timerId = null
    this.previousLoading = false
    this.previousPolling = false

    this.getData.bind(this)
  }

  shouldComponentUpdate (nextProps) {
    const nextActiveTab = resolvePath(nextProps, 'current.navigator.activeTab')
    const currentActiveTab = resolvePath(this.props, 'current.navigator.activeTab')

    return nextProps.initializing !== this.props.initializing ||
      currentActiveTab !== nextActiveTab
  }

  componentDidMount () {
    if (resolvePath(this.props, 'match.params.username', '') === '_') {
      this.props.history.push(`/u/${this.props.auth.username}/devices/${this.props.match.params.deviceId}`)
      return
    }
    window.requestAnimationFrame(() => {
      this.stopPulling()
      this.getData().then(() => this.startPulling())
    })
  }

  componentWillUnmount () {
    this.stopPulling()
  }

  componentDidUpdate (prevProps) {
    this.previousLoading = prevProps.loading
    this.previousPolling = prevProps.initializing

    const nextRevision = parseInt(this.props.match.params.revision, 10)
    const currentRevision = parseInt(prevProps.match.params.revision, 10)
    const nextDevice = this.props.match.params.deviceId
    const currentDevice = prevProps.match.params.deviceId
    const differentDevice = currentDevice && nextDevice && currentDevice !== nextDevice
    const differentRevision = !isNaN(nextRevision) && !isNaN(currentRevision) && nextRevision !== currentRevision
    if (differentRevision || differentDevice) {
      this.stopPulling()
      this.getData().then(() => this.startPulling())
    }
  }

  getData = (refresher = false) => {
    const { init, auth, match, current } = this.props
    const { timestamp, device } = current
    const { deviceId, revision } = match.params
    const { token } = auth

    if (this.updater.isMounted(this) === false) {
      this.stopPulling()
      return Promise.resolve()
    }

    if (this.props.current.editing === true || this.props.initializing === true) {
      return Promise.resolve()
    }

    return init(token, timestamp, deviceId, revision, device, refresher)
  }

  startPulling = () => {
    const intervalSize = Number(GetEnv('REACT_APP_REFRESH_RATE', '3000'))
    this.timerId = setInterval(() => {
      this.getData(true)
    }, intervalSize)
  }

  stopPulling = () => {
    if (this.timerId !== null) {
      window.clearInterval(this.timerId)
    }
    this.timerId = null
    window.__PH_CDLLETS = undefined
  }

  render () {
    const {
      onChangeNavigatorTab,
      onSetEditing,
      onSetLogsFilter,
      match,
      auth,
      dispatch,
      current,
      loading,
      saving: devsSaving
    } = this.props
    const { deviceId } = match.params
    const { device, navigator } = current
    const { username, token } = auth
    const { activeTab } = navigator

    const saving = (devsSaving || {})[deviceId]

    return <React.Fragment>
      <DevicesBreadcumbs
        key="breadcrumbs"
        username={match.params.username || username}
        deviceNick={device ? device.nick : deviceId}
      />
      <div key="deviceBody" className="row">
        <div className="col-md-12">
          {loading
            ? (
              <Loading className="pt-6" key="spinner" />
            )
            : (
              <UserDeviceInner
                match={this.props.match}
                device={device || {}}
                deviceId={deviceId}
                disabled={loading}
                dispatch={dispatch}
                username={username}
                token={token}
                navigator={navigator}
                onChangeNavigatorTab={onChangeNavigatorTab}
                onSetEditing={onSetEditing}
                onSetLogsFilter={onSetLogsFilter}
                activeTab={activeTab}
                saving={saving}
              />
            )}
        </div>
      </div>
    </React.Fragment>
  }
}

export default connect(
  state => ({
    current: state.devs.current,
    loading: state.devs.loading,
    refreshing: state.devs.refreshing,
    saving: state.devs.devsSaving,
    auth: state.auth,
    initializing: state.devs.initializing
  }),
  (dispatch, props) => ({
    init: (...params) => dispatch(initializeDevice(...params)),
    onChangeNavigatorTab: tab => () => dispatch(deviceNavigatorSetTab(tab)),
    onSetLogsFilter: logsFilter => dispatch(deviceSetLogsFilter(logsFilter)),
    onSetEditing: editing => dispatch(deviceSetEditing(editing))
  })
)(withRouter(UserDevice))
