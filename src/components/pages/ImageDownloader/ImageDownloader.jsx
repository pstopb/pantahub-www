/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { connect } from 'react-redux'
import omit from 'lodash.omit'

import { getImagesData, IMAGES_CI_URL } from '../../../services/images.service'
import Loading from '../../atoms/Loading/Loading'
import LoadingButton from '../../atoms/LoadingButton/LoadingButton'
import { DownloadImageWithAuth } from '../../../store/images/actions'
import CheckInput from '../../atoms/CheckInput/CheckInput'
import { Breadcumbs } from '../../atoms/Breadcumbs/Breadcumbs'
import { userDashboardPath } from '../../../router/routes'

const STATUSES = {
  LOADING: 'l',
  NONE: 'n',
  SUCESS: 's',
  FAILURE: 'f',
  DOWNLOADING: 'd'
}

class ImageDownloader extends React.Component {
  constructor (props) {
    super(props)

    const searchParams = new URLSearchParams(window.location.search)

    const source = searchParams.get('source')
      ? searchParams.get('source')
      : IMAGES_CI_URL

    const meta = searchParams.get('meta')
      ? JSON.parse(searchParams.get('meta') || '{}')
      : null

    const cmdline = searchParams.get('cmdline')
      ? searchParams.get('cmdline')
      : undefined

    this.state = {
      source: source,
      channels: ['Loading channels...'],
      channel: '',
      devices: [],
      meta: meta,
      cmdline: cmdline,
      selectedDevice: null,
      error: null,
      status: STATUSES.NONE,
      wifi: {
        ssid: null,
        password: null
      },
      autoClaimDevice: !(searchParams.get('autoclaim-device') === 'false') || searchParams.get('seteable-meta') === 'true',
      seteableMeta: searchParams.get('seteable-meta') === 'true',
      disableMeta: false,
      seteableWifi: searchParams.get('seteable-wifi') === 'true'
    }
  }

  componentDidMount () {
    this.getChannels(this.state.source)
  }

  getChannels = async (src) => {
    try {
      this.setState({ status: STATUSES.LOADING })
      const resp = await getImagesData(src)
      if (!resp.ok) {
        throw Error('Error getting source json')
      }

      const channelDevices = Object.keys(resp.json).reduce((acc, key) => {
        if (resp.json[key].length > 0) {
          acc.channels.push(key)
          acc.devices[key] = []
          resp.json[key][0].devices.forEach(d => {
            d.images.forEach(i => {
              acc.devices[key].push({
                ...omit(d, ['images']),
                ...i
              })
            })
          })
        }
        return acc
      }, { channels: [], devices: {} })

      const channel = channelDevices.channels.length > 0
        ? channelDevices.channels.find((c) => c === 'stable') || channelDevices.channels[0]
        : ''

      const selectedDevice = channel !== '' && channelDevices.devices[channel][0]

      return this.setState({
        status: STATUSES.SUCESS,
        channels: channelDevices.channels,
        devices: channelDevices.devices,
        selectedDevice: selectedDevice,
        channel: channel,
        source: src
      })
    } catch (e) {
      return this.setState({
        status: STATUSES.FAILURE,
        channel: '',
        error: e
      })
    }
  }

  setSource = (event) => {
    this.getChannels(event.target.value)
  }

  setChannel = (event) => {
    const value = event.target.value
    this.setState(state => ({ ...state, channel: value }))
  }

  selectDevice = (event) => {
    const index = event.target.value
    const selectedDevice = this.state.devices[this.state.channel][index]
    this.setState(state => ({ ...state, selectedDevice }))
  }

  getMeta = () => {
    try {
      if (!this.state.meta) {
        return ''
      }
      return JSON.stringify(this.state.meta, null, '\t')
    } catch (e) {
      return ''
    }
  }

  setMeta = (event) => {
    try {
      if (event.target.value.trim() === '') {
        this.setState(state => ({ ...state, meta: null }))
        return
      }
      const meta = JSON.parse(event.target.value)
      this.setState(state => ({ ...state, meta }))
    } catch (e) {
      console.warn(e)
    }
  }

  downloadImage = (event) => {
    event.preventDefault()
    this.setState({ status: STATUSES.DOWNLOADING })
    const bucket = this.state.source.match(/https:\/\/(.*?)\./)[1]
    const project = this.state.source.match(/https:\/\/[^/]*\/(.*)\/[^/]*\.json/)[1]
    const releaseindex = this.state.source.match(/https:\/\/[^/]*\/.*\/([^/]*)\.json/)[1]
    const variant = this.state.selectedDevice.name.indexOf('installer') >= 0
      ? 'installer'
      : undefined

    const payload = this.state.meta && this.state.autoClaimDevice && this.state.seteableMeta
      ? { 'default-user-meta': this.state.meta }
      : null

    this.props.DownloadImageWithAuth({
      device: this.state.selectedDevice.target,
      devicenick: this.state.selectedDevice.devicenick,
      channel: this.state.channel,
      bucket: bucket,
      project: project,
      releaseindex: releaseindex,
      payload: payload,
      variant: variant,
      wifiConfig: this.state.wifi,
      cmdline: this.state.cmdline,
      autoClaim: this.state.autoClaimDevice,
      source: this.state.source
    })

    setTimeout(() => {
      this.setState({ status: STATUSES.NONE })
    }, 400)
  }

  updateSeteableWifi = (val) => {
    this.setState({ seteableWifi: val })
  }

  updateAutoClaim = (val) => {
    const newState = { autoClaimDevice: val }

    if (!val) {
      newState.disableMeta = true
    } else {
      newState.disableMeta = false
    }

    this.setState(newState)
  }

  updateSeteableMeta = (val) => {
    const newState = { seteableMeta: val }

    if (val && !this.state.autoClaimDevice) {
      newState.autoClaimDevice = true
    }
    this.setState(newState)
  }

  setWifi = (part) => (evnt) => {
    this.setState({
      wifi: {
        ...this.state.wifi,
        [part]: evnt.target.value
      }
    })
  }

  render () {
    const username = this.props?.auth?.username

    if (this.state.status === STATUSES.LOADING) {
      return (
        <div className="row">
          <header className="col-12">
            <Breadcumbs
              steps={[
                {
                  to: `${userDashboardPath}/${username}`,
                  title: 'Dashboard'
                },
                {
                  to: '',
                  title: 'Download pantavisor'
                }
              ]}
            />
          </header>
          <section className="col-12 mt-10">
            <div className="row">
              <div className="col-md-6">
                <div className="form-group pb-2">
                  <label
                    htmlFor="source"
                  >
                    Image Source
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="source"
                    aria-describedby="sourceHelp"
                    placeholder="Enter your custom image source"
                    value={this.state.source}
                    onChange={this.setSource}
                  />
                  <small
                    id="sourceHelp"
                    className="form-text text-muted"
                  >
                    {'Any other Pantacor CI image json compatible URL.'}
                  </small>
                </div>
                <div className="pt-90 pb-90">
                  <Loading />
                </div>
              </div>
            </div>
          </section>
        </div>
      )
    }
    return (
      <div className="row">
        <header className="col-12">
          <Breadcumbs
            steps={[
              {
                to: `${userDashboardPath}/${username}`,
                title: 'Dashboard'
              },
              {
                to: '',
                title: 'Download pantavisor'
              }
            ]}
          />
        </header>
        <section className="col-12 mt-10">
          <div className="row">
            <div className="col-md-6">
              <div className="form-group pb-2">
                <label
                  htmlFor="source"
                >
                  Image Source
                </label>
                <input
                  type="url"
                  className="form-control"
                  id="source"
                  aria-describedby="sourceHelp"
                  placeholder="Enter your custom image source"
                  value={this.state.source}
                  onChange={this.setSource}
                />
                <small
                  id="sourceHelp"
                  className="form-text text-muted pl-1"
                >
                  {'You can use any other Pantacor CI image json compatible URL.'}
                </small>
              </div>
              <div className="form-group pb-2">
                <label
                  htmlFor="channelSelect"
                >
                  Select channel
                </label>
                <select
                  className="form-control"
                  id="channelSelect"
                  value={this.state.channel}
                  onChange={this.setChannel}
                >
                  {this.state.channels.map((o) => (
                    <option key={o}>
                      {o}
                    </option>
                  ))}
                </select>
              </div>
              {this.state.channel !== '' && this.state.devices[this.state.channel] && (
                <div className="form-group pb-2">
                  <label
                    htmlFor="deviceSelect"
                  >
                    Select device
                  </label>
                  <select
                    className="form-control"
                    id="deviceSelect"
                    onChange={this.selectDevice}
                  >
                    {this.state.devices[this.state.channel].map((o, index) => (
                      <option value={index} key={index}>
                        {o.target}
                        {o.name && o.name !== 'default' && `- ${o.name}`}
                        &nbsp;({o.devicenick})
                      </option>
                    ))}
                  </select>
                </div>
              )}
              <CheckInput
                value={this.state.autoClaimDevice}
                onChange={this.updateAutoClaim}
              >
                Pair with this account on first boot
              </CheckInput>
              <CheckInput
                value={this.state.seteableWifi}
                onChange={this.updateSeteableWifi}
              >
                Configure Wifi connection
              </CheckInput>
              {this.state.seteableWifi && (
                <>
                  <div className="form-group pb-2">
                    <label
                      htmlFor="ssid"
                    >
                      Wifi SSID
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="ssid"
                      aria-describedby="ssidHelp"
                      placeholder="Enter you Wifi SSID"
                      value={this.state.wifi.ssid}
                      onChange={this.setWifi('ssid')}
                    />
                  </div>
                  <div className="form-group pb-2">
                    <label
                      htmlFor="password"
                    >
                      Wifi Password
                    </label>
                    <input
                      type="text"
                      className="form-control"
                      id="password"
                      aria-describedby="passwordHelp"
                      placeholder="Enter you Wifi password"
                      value={this.state.wifi.password}
                      onChange={this.setWifi('password')}
                    />
                  </div>
                </>
              )}
              <CheckInput
                value={this.state.seteableMeta}
                disabled={this.state.disableMeta}
                onChange={this.updateSeteableMeta}
              >
                Preload device configuration (user-meta)
              </CheckInput>
              {this.state.seteableMeta && (
                <div className="form-group pb-2">
                  <label
                    htmlFor="devicemeta"
                  >
                    Set device user-meta
                  </label>
                  <textarea
                    className="form-control"
                    id="devicemeta"
                    onChange={this.setMeta}
                    disabled={this.state.disableMeta}
                    defaultValue={this.getMeta()}
                  />
                  <div id="devicemetaHelp" className="form-text">
                    Write a full json for configuration
                  </div>
                </div>
              )}
              {!!this.state.selectedDevice && (
                <div className="form-group pb-2 mt-60">
                  <LoadingButton
                    loading={this.state.status === STATUSES.DOWNLOADING}
                    loadingChild={'Starting download...'}
                    className="btn btn-primary"
                    onClick={this.downloadImage}
                  >
                    Download {this.state.selectedDevice.target}
                  </LoadingButton>
                </div>
              )}
            </div>
          </div>
        </section>
      </div>
    )
  }
}

export default connect(
  (state) => state.auth,
  { DownloadImageWithAuth }
)(ImageDownloader)
