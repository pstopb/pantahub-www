/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { Track } from '../../../lib/analytics.helper'

export function Tracker ({ event, payload = {}, children }) {
  const trackOnClick = (e) => {
    Track(event, payload)
  }

  return (
    <span onClick={trackOnClick}>
      {children}
    </span>
  )
}

export const TrackerWrapper = (Component, displayName) => {
  const Wrapped = (props) => {
    const { event, payload = {}, children, ...rest } = props

    const trackOnClick = (e) => {
      Track(event, payload, e)
      if (props.onClick) {
        props.onClick(e)
      }
    }

    return (
      <Component
        {...rest}
        onClick={trackOnClick}
      >
        {children}
      </Component>
    )
  }

  Wrapped.displayName = displayName || Component.displayName

  return Wrapped
}

function Button (props) {
  const { children } = props
  return (
    <button {...props}>
      {children}
    </button>
  )
}

export const TrackedButton = TrackerWrapper(Button, 'TrackedButton')
