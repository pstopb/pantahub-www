import React from 'react'
import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'

dayjs.extend(relativeTime)

export default function RelativeTime ({ when }) {
  const date = dayjs(when)
  return (
    <time dateTime={date.format()}>
      <span title={date.format()}>
        {date.fromNow()}
      </span>
    </time>
  )
}
