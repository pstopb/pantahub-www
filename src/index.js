/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { register, unregister } from './registerServiceWorker'
import { GetEnv } from './lib/const.helpers'

ReactDOM.render(<App />, document.getElementById('root'))

if (GetEnv('REACT_APP_SERVICE_WORKER', 'false') === 'true') {
  register()
} else {
  if (navigator && navigator.serviceWorker && navigator.serviceWorker.getRegistrations) {
    navigator.serviceWorker.getRegistrations()
      .then(rs =>
        rs.forEach(r =>
          r.unregister().then(t => t && console.info('Service Worker unregistered'))
        )
      )
  }
  unregister()
}

function GoogleTagManagerStarter (w, d, s, l, i) {
  w[l] = w[l] || []
  w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' })
  const f = d.getElementsByTagName(s)[0]
  const j = d.createElement(s)
  const dl = l !== 'dataLayer' ? '&l=' + l : ''
  j.async = true
  j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl
  f.parentNode.insertBefore(j, f)
}

if (GetEnv('REACT_APP_GTM_ID')) {
  GoogleTagManagerStarter(window, document, 'script', 'dataLayer', GetEnv('REACT_APP_GTM_ID'))
}

import('mixpanel-browser').then(mixpanel => {
  if (GetEnv('REACT_APP_MIXPANEL_ID')) {
    window.mixpanel = mixpanel.default
    window.mixpanel.init(GetEnv('REACT_APP_MIXPANEL_ID'))
  }
})
