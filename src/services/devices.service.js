/*
 * Copyright (c) 2017-2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import last from 'lodash.last'
import merge from 'lodash.merge'
import sortBy from 'lodash.sortby'
import wcmatch from 'wildcard-match'
import {
  _getJSON,
  _delete,
  _patchJSON,
  _putJSON,
  _postJSON,
  _requestContentType
} from './api.service'

import { GetApiURL, GetEnv } from '../lib/const.helpers'
import { throwErrorIf, AUTH_ERRORS } from '../lib/api.helpers'
import { resolvePath } from '../lib/object.helpers'
import { PropsToQueryString } from '../lib/utils'

const DEVS_URL = `${GetApiURL()}/devices`

const SUMMARY_URL = `${GetApiURL()}/trails/summary`

const trailPostUrl = (deviceId) =>
  `${GetApiURL()}/trails/${deviceId}/steps/`

const trailObjectsURL = (id, rev) =>
  `${GetApiURL()}/trails/${id}/steps/${rev}/objects`

const tailsDeviceSummaryUrl = (id) =>
  `${GetApiURL()}/trails/${id}/summary`

const devLogsURL = (id, before = null, after = null, page = 500, sort = '-time-created', rev = null, plat = null, src = null, cursor = null) =>
  `${GetApiURL()}/logs/?dev=prn:::devices:/${id}` +
    (before ? `&before=${before}` : '') +
    (after ? `&after=${after}` : '') +
    (page ? `&page=${page}` : '') +
    (sort ? `&sort=${sort}` : '') +
    (rev !== null ? `&rev=${rev}` : '') +
    (plat ? `&plat=${plat}` : '') +
    (src ? `&src=${src}` : '') +
    (cursor ? `&cursor=${cursor}` : '')

const devLogsCursorURL = (nextCursor) =>
  `${GetApiURL()}/logs/cursor`

const devStepsURL = (id, status = '') =>
  `${GetApiURL()}/trails/${id}/steps?progress.status=${status}`

const keyBlacklist = ['#spec']
const expandableExtensions = ['json']
const isSignature = (element = '') => {
  return typeof element === 'object' &&
    !Array.isArray(element) &&
    element['#spec'] === 'pvs@2'
}

function isUploaded (summaryResponse) {
  return resolvePath(summaryResponse, 'json.state-sha', '').length > 0
}

// Device API
export const getDevsData = async token => _getJSON(SUMMARY_URL, token)

export const getDevices = async (token, props) =>
  _getJSON(`${DEVS_URL}/${PropsToQueryString(props)}`, token)

export const getDeviceData = async (token, id) =>
  _getJSON(`${DEVS_URL}/${id}`, token)

export const getDeviceSummary = async (token, id) =>
  _getJSON(tailsDeviceSummaryUrl(id), token)

export const getDeviceSteps = async (token, id, status) =>
  _getJSON(devStepsURL(id, status), token)

export const getDeviceObjects = async (token, id, rev) =>
  _getJSON(trailObjectsURL(id, rev), token)

export const getDeviceLogs = async (
  token,
  id,
  before,
  after = '1970-01-01T00:00:00.0Z',
  page = 2000,
  sort = '-time-created',
  rev = null,
  plat = null,
  src = null,
  nextCursor = null
) => {
  if (nextCursor) {
    // use existing cursor
    return _postJSON(devLogsCursorURL(), token, { 'next-cursor': nextCursor })
  } else {
    // request a new cursor
    return _getJSON(devLogsURL(id, before, after, page, sort, rev, plat, src, 'true'), token)
  }
}

export const deleteDevice = async (token, id) =>
  _delete(`${DEVS_URL}/${id}`, token)

export const publishDevice = async (token, id) =>
  _putJSON(`${DEVS_URL}/${id}/public`, token)

export const unPublishDevice = async (token, id) =>
  _delete(`${DEVS_URL}/${id}/public`, token)

export const claimDevice = async (token, id, secret) =>
  _putJSON(`${DEVS_URL}/${id}?challenge=${secret}`, token)

export const setDeviceMetadata = async (token, id, meta, type = 'user-meta') =>
  _putJSON(`${DEVS_URL}/${id}/${type}`, token, meta)

export const getFactoryToken = async (token, body) =>
  _postJSON(`${DEVS_URL}/tokens`, token, body)

export const fetchFile = async (token, url) =>
  _requestContentType(url, token, 'text/plain; charset=utf-8')

export const patchDevice = async (token, id, payload) =>
  _patchJSON(`${DEVS_URL}/${id}`, token, payload)

export const postTrails = async (token, id, payload) =>
  _postJSON(trailPostUrl(id), token, payload)

async function getLogsData (token, deviceId, fromRefresher, before, after, rev = null, plat = null, src = null, nextCursor = null, fromFuture = true) {
  const logsResponse = await getDeviceLogs(
    token,
    deviceId,
    before,
    after, // fromRefresher ? window.__PH_CDLLETS : undefined, // after
    fromRefresher
      ? parseInt(GetEnv('REACT_APP_LOGS_WARM_CHUNK', 100))
      : parseInt(GetEnv('REACT_APP_LOGS_COLD_CHUNK', 200)), // page
    fromFuture ? 'time-created' : '-time-created', // sort
    rev,
    plat,
    src,
    nextCursor
  )

  throwErrorIf(logsResponse)(AUTH_ERRORS)

  return {
    newLogs: resolvePath(logsResponse, 'json.entries', []),
    nextCursor: resolvePath(logsResponse, 'json.next-cursor')
  }
}

export async function getDeviceLogsService (token, deviceId, fromRefresher, before, after, rev = null, plat = null, src = null, cursor = null, fromFuture = true) {
  const response = await getLogsData(token, deviceId, fromRefresher, before, after, rev, plat, src, (cursor.valid ? cursor.id : null), fromFuture)
  const { newLogs } = response

  const devInitNewLogs = sortBy(newLogs || [], 'time-created')

  return {
    newLogs: devInitNewLogs,
    nextCursor: response.nextCursor,
    fromRefresher,
    fromFuture
  }
}

async function getAllDataFromApi (args) {
  const { token, deviceId, device, refresh, revision } = args
  let devResponse = {}
  let stepsResponse = {}
  let summaryResponse = {}
  let stateObjectsResponse = {}
  if (!refresh) {
    return {
      dev: device,
      steps: device.history.steps
    }
  }

  // fetch device information and enrich it
  [devResponse, summaryResponse] = await Promise.all([
    getDeviceData(token, deviceId),
    getDeviceSummary(token, deviceId)
  ])

  const rev = revision || resolvePath(summaryResponse, 'json.progress-revision')

  throwErrorIf(devResponse, summaryResponse)(AUTH_ERRORS)

  // Only Load steps and logs if device upload all files
  if (isUploaded(summaryResponse)) {
    [stepsResponse, stateObjectsResponse] = await Promise.all([
      getDeviceSteps(token, deviceId, '{"$ne":""}'),
      getDeviceObjects(token, deviceId, rev)
    ])
  }

  throwErrorIf(stepsResponse, stateObjectsResponse)(AUTH_ERRORS)

  return {
    dev: devResponse.json,
    summary: summaryResponse.json,
    steps: resolvePath(stepsResponse, 'json', []),
    objects: resolvePath(stateObjectsResponse, 'json', [])
  }
}

function findLatestStep (steps = []) {
  return steps.reduce((acc, val) => val.rev > (acc.rev || -1) ? val : acc, {})
}

function getRevisions (steps = [], revision) {
  const lastRevision = findLatestStep(steps).rev
  const parsedRevision = parseInt(revision, 10)
  const rev = isNaN(parsedRevision) ? lastRevision : parsedRevision
  const revIndex = steps.findIndex(x => x.rev === rev)
  return {
    lastRevision,
    rev,
    revIndex
  }
}

const reduceStatePart = (acc, [key, value]) => {
  const extension = last((key || '').split('.'))
  const isExpandable = expandableExtensions.indexOf(extension) >= 0
  acc[key] = {
    extension,
    isExpandable,
    hidden: keyBlacklist.indexOf(key) !== -1,
    value
  }

  return acc
}

export const ReduceSignatures = (acc, [key, value]) => {
  if (!isSignature(value)) {
    acc.include = acc.include || []
    acc.exclude = acc.exclude || []
    return acc
  }

  try {
    const v = window.atob(value.protected)
    const json = JSON.parse(v)
    const include = json.pvs.include.map((rule) => ({
      pattern: wcmatch(rule),
      sig: key
    }))
    const exclude = json.pvs.exclude.map((rule) => ({
      pattern: wcmatch(rule),
      sig: key
    }))
    acc.include = (acc.include || []).concat(include)
    acc.exclude = (acc.exclude || []).concat(exclude)
  } catch (e) {
    console.info('====================================')
    console.info(e)
  }
  return acc
}

export const SetSignatures = (signatures) => (acc, [key, spec]) => {
  const excluded = signatures.exclude.findIndex((e) => {
    return e.pattern(key)
  })
  if (excluded >= 0) {
    acc[key] = {
      ...spec,
      signatures: []
    }

    return acc
  }

  const included = signatures.include.findIndex((e) => {
    return e.pattern(key)
  })
  if (included >= 0) {
    signatures.include.forEach((e) => {
      if (e.pattern(key)) {
        acc[key] = {
          ...spec,
          signatures: resolvePath(acc, `${key}.signatures`, []).concat(e.sig)
        }
      }
    })

    return acc
  }

  acc[key] = { ...spec, signatures: [] }
  return acc
}

const reduceUrls = (stateURLs) => (acc, [key, spec]) => {
  acc[key] = {
    url: (stateURLs[spec.value] || {}).url,
    size: (stateURLs[spec.value] || {}).size,
    ...spec
  }
  return acc
}

const getStateUrls = (objects) => {
  // TODO: implement an objects cache to only request the new or missing objects here
  // or to completely omit this request if there were no changes in files between revisions
  return objects.reduce((acc, object) => {
    // FIXME: use env var here
    acc[object.id] = { url: `${object['signed-geturl']}`, size: object.size }
    return acc
  }, {})
}

const transformState = (state, objects, signatures) => {
  let newState = Object.entries(state).reduce(reduceStatePart, {})
  newState = Object.entries(newState).reduce(reduceUrls(getStateUrls(objects)), {})
  newState = Object.entries(newState).reduce(SetSignatures(signatures), {})

  return newState
}

export function buildCurrentStep (steps, objects = [], revIndex) {
  const currentStep = Object.assign({}, resolvePath(steps, `${revIndex}`, {}))

  currentStep.rawState = merge({}, currentStep.state)
  currentStep.signatures = Object.entries(currentStep.rawState).reduce(ReduceSignatures, { include: [], exclude: [] })
  currentStep.state = transformState(currentStep.state, objects, currentStep.signatures)

  return currentStep
}

function consolidateDeviceData (args) {
  const { dev, lastRevision, steps, rev, revIndex, currentStep, summary } = args
  return Object.assign({}, dev, {
    summary,
    history: {
      revision: rev,
      lastRevision,
      steps,
      revIndex,
      currentStep
    }
  })
}

export async function getAllDeviceData (args) {
  const {
    revision
  } = args
  const {
    dev,
    steps,
    summary,
    objects
  } = await getAllDataFromApi(args)
  const { lastRevision, rev, revIndex } = getRevisions(steps, revision)
  const currentStep = buildCurrentStep(steps, objects, revIndex)
  const consolidatedDevice = consolidateDeviceData({
    dev,
    lastRevision,
    rev,
    revIndex,
    currentStep,
    steps,
    summary
  })
  return {
    consolidatedDevice
  }
}
