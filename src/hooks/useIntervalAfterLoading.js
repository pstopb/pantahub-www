import { useRef, useEffect } from 'react'

const REFRESH_RATE = Number(window.env.REACT_APP_REFRESH_RATE || '4000')

function usePrevious (value) {
  const ref = useRef()
  useEffect(() => {
    ref.current = value
  })
  return ref.current
}

export function useIntervalAfterLoading (callback, loading = false, delay = REFRESH_RATE) {
  const savedCallback = useRef(null)
  const timeout = useRef(null)
  const stop = useRef(false)
  const valueOfValidation = loading
  const previousValidation = usePrevious(valueOfValidation)

  const clearInterval = () => {
    stop.current = true
    if (timeout.current !== null) {
      window.clearTimeout(timeout.current)
      timeout.current = null
    }
  }
  const tick = () => {
    if (savedCallback.current) { savedCallback.current() }
  }

  useEffect(() => {
    savedCallback.current = callback
    stop.current = false
  })

  useEffect(() => {
    tick()
  }, [])

  useEffect(() => {
    if (previousValidation && !valueOfValidation && delay !== null && !stop.current) {
      timeout.current = setTimeout(() => tick(), delay)
    }
    return () => {
      clearInterval()
    }
  }, [delay, valueOfValidation])

  return {
    tick,
    previousValidation,
    clearInterval
  }
}
