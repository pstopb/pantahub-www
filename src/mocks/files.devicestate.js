export const FileDeviceState = {
  '#spec': {
    url: null,
    extension: '#spec',
    isExpandable: false,
    hidden: true,
    value: 'pantavisor-service-system@1'
  },
  '_config/pvr-sdk/etc/pvr-sdk/config.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      httpd: {
        listen: '0.0.0.0',
        port: '12368'
      }
    }
  },
  '_hostconfig/pvr/docker.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      platforms: [
        'linux/arm64',
        'linux/arm'
      ]
    }
  },
  'awconnect/lxc.container.conf': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiI5N2I5N2EyYWVjOTVmYzhhMTI0NjAwNTk4ZmZlYWE2NmYyMGM3ZjRkNGE1NWMzY2FhZDhmNTg2NzQ0NGM1ZWZiIiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoiYXdjb25uZWN0L2x4Yy5jb250YWluZXIuY29uZiIsIlNpemUiOjY5NSwiTWV0aG9kIjoiR0VUIiwiU2hhIjoiMTUzZDU4NTg4YjAzMjdmNzNjODQyNGMyMTRjMDM5ZmNkZDk3NTgxNGJjMDc1YmM1YzcyZjgyZmQzY2RmZDdiNiJ9.YCjW3FDUwCE8Dn7q3K8PesDl2jGNstGX7t_0s2VmCK8',
    extension: 'conf',
    isExpandable: false,
    hidden: false,
    value: '153d58588b0327f73c8424c214c039fcdd975814bc075bc5c72f82fd3cdfd7b6'
  },
  'awconnect/root.squashfs': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiI2MGJlZDg4ZTA5OGRkYTA0NDY4YTlkZTY5OTU4ZmFiNjdjNjQ5OThhZmQwMjk5MzQxYjU0NmNlNjIzZTBiMTE5IiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoiYXdjb25uZWN0L3Jvb3Quc3F1YXNoZnMiLCJTaXplIjoxMDgxMDk4MjQsIk1ldGhvZCI6IkdFVCIsIlNoYSI6Ijk5ODliNjJkNjljOTFhYjQyNmYwMjY2NTUyMWEwMzVjMGZkMDEwNTVkNDVlMjY5NWIxZDZhOWZkNzEwMmU3NmUifQ.f5Ho43rBoyrpxZcnGiWJNMQo0-uOePFZ24D6SrtAHSQ',
    extension: 'squashfs',
    isExpandable: false,
    hidden: false,
    value: '9989b62d69c91ab426f02665521a035c0fd01055d45e2695b1d6a9fd7102e76e'
  },
  'awconnect/root.squashfs.docker-digest': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJiM2ZkMjE3MmYzNmYxZGFjZTJkY2IyZjg1M2QwZmMzNzZhNmExM2VjN2IzZmY2MzllMGFiMWJiYzFjMDAwNmU1IiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoiYXdjb25uZWN0L3Jvb3Quc3F1YXNoZnMuZG9ja2VyLWRpZ2VzdCIsIlNpemUiOjEyNiwiTWV0aG9kIjoiR0VUIiwiU2hhIjoiMDQ3ZGVmNjlmODVkYmYwNDI1NDI3OTU4MzRlMWFkZTE1ZTlhNGI3ZTc3NTUyNmJlZDM0M2NlNGYxNzdhNjYyYyJ9.JXd0Jvj_vbJN90UMww9_pCzM6Z_jSJBFUOC7ZNssIDU',
    extension: 'docker-digest',
    isExpandable: false,
    hidden: false,
    value: '047def69f85dbf042542795834e1ade15e9a4b7e775526bed343ce4f177a662c'
  },
  'awconnect/run.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      '#spec': 'service-manifest-run@1',
      config: 'lxc.container.conf',
      name: 'awconnect',
      'root-volume': 'root.squashfs',
      storage: {
        'docker--etc-NetworkManager-system-connections': {
          persistence: 'permanent'
        },
        'lxc-overlay': {
          persistence: 'boot'
        }
      },
      type: 'lxc',
      volumes: []
    }
  },
  'awconnect/src.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      '#spec': 'service-manifest-src@1',
      args: {},
      config: {},
      docker_digest: 'registry.gitlab.com/pantacor/pv-platforms/wifi-connect@sha256:5c889720c6243408049b7a2f9ec75b9f4e376f6cbc28e39e091420a1e19df2aa',
      docker_name: 'registry.gitlab.com/pantacor/pv-platforms/wifi-connect',
      docker_source: 'remote,local',
      docker_tag: 'arm32v5',
      persistence: {},
      template: 'builtin-lxc-docker'
    }
  },
  'bsp/addon-plymouth.cpio.xz4': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiIzNzVjMGVhOGU3MmFmOGJlN2Y2MDgyZWY1YjFlNDI4M2IyMzIzMmE3NTg2OGNhOWEyMGZiMjZkNmMzNmI2N2YxIiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoiYnNwL2FkZG9uLXBseW1vdXRoLmNwaW8ueHo0IiwiU2l6ZSI6MTMwMDI0ODQsIk1ldGhvZCI6IkdFVCIsIlNoYSI6ImJlYWU2YTdiYjIzNTkxNmNhYzUyYmNmZWNlNjRjMzA2MTVjZGVkOGM0YzY0MGU2OTQxZTdlY2FiZTUzYjQ5MjAifQ.Q4dIgfV46K1ZYtXEW-u_5zIrGO9NZr2Aw_iFJ9ncguk',
    extension: 'xz4',
    isExpandable: false,
    hidden: false,
    value: 'beae6a7bb235916cac52bcfece64c30615cded8c4c640e6941e7ecabe53b4920'
  },
  'bsp/build.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      altrepogroups: '',
      branch: 'master',
      commit: '7d5ba78761e4d880c3403d642187bfdc93e49683',
      gitdescribe: '014-rc9-12-g7d5ba78',
      pipeline: '355967648',
      platform: 'rpi64',
      project: 'pantacor/pv-manifest',
      pvrversion: 'pvr version 022-15-g26ebb342',
      target: 'arm-rpi64',
      time: '2021-08-19 15:08:44 +0000'
    }
  },
  'bsp/firmware.squashfs': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiIwMDNmMzNiNTBjMGY3NjMwYTIyMjBhZDNkMGExMWExZGExYWU2NjQwZTQzOTE2OWY2NWZjMTRhZGRjZTU2MDNjIiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoiYnNwL2Zpcm13YXJlLnNxdWFzaGZzIiwiU2l6ZSI6MjM3NzcyODAsIk1ldGhvZCI6IkdFVCIsIlNoYSI6ImM5NjhhNjc0ZDEyMjU4ZjAwZjRkOTI1MTYzNzA2NWEwNGFiZjdmOTUyODUzMDhiZmNhN2U0ZjZjY2Y5ZGU3YzUifQ.GtG0BsEDqO19ljKNvT1LFpNcfACBKM4N-D39oFcPKSA',
    extension: 'squashfs',
    isExpandable: false,
    hidden: false,
    value: 'c968a674d12258f00f4d9251637065a04abf7f95285308bfca7e4f6ccf9de7c5'
  },
  'bsp/kernel.img': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiIxY2ZhYjVmM2JkZGM5MDYyYWI3MzhkYWEzN2FiNjExOGYzYzVkNDhhNGY1NDUxZmU3YzQ5ZGU2NDE0MDJlYjAzIiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoiYnNwL2tlcm5lbC5pbWciLCJTaXplIjoxNTI5MjkyOCwiTWV0aG9kIjoiR0VUIiwiU2hhIjoiYjU5NDM4ZTRjYjBkYjExNjg5NjAxZTdlMjZlOGRjNmRhZDBiNTAwNzA3MmVkYmYxMGU4ODZhOGRkNTFkMjM5NyJ9.PlwKgcYukOKqhGopJRiXaEILnguMBwvx9T7K7Xix2_o',
    extension: 'img',
    isExpandable: false,
    hidden: false,
    value: 'b59438e4cb0db11689601e7e26e8dc6dad0b5007072edbf10e886a8dd51d2397'
  },
  'bsp/modules.squashfs': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJmZjI0MjQ3MThjNDIyZmU3YjlhNWUzMDM3MGJhM2YyZjFjMzQwMTQ1ODIzMDhhNjRkZThkOGM3MmJjOWFjNmQ4IiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoiYnNwL21vZHVsZXMuc3F1YXNoZnMiLCJTaXplIjoxNTU0ODQxNiwiTWV0aG9kIjoiR0VUIiwiU2hhIjoiMzg1ZjA0ZmNhNTU1OTEyZjg2NTUwMThmOTdjZDA5YTI1MDJmZGQ3OWFmYzE0Y2Q1ZmQ1NzY4MmQwYTJjZjRlMCJ9.wxfDbQlc_iyZiBBG2UmsHQypO0HoCXYRuJDO-CT1BHw',
    extension: 'squashfs',
    isExpandable: false,
    hidden: false,
    value: '385f04fca555912f8655018f97cd09a2502fdd79afc14cd5fd57682d0a2cf4e0'
  },
  'bsp/pantavisor': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjYmIyMjIzNWZhOTdiOTllMjhkZTM0MzQ0NjJjZmYzMjllMGRiYmUzOGJmOTA3YzlkYzRiOTk2ZGZjZmEwMjI3IiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoiYnNwL3BhbnRhdmlzb3IiLCJTaXplIjozMjE2MjI0LCJNZXRob2QiOiJHRVQiLCJTaGEiOiI0MDVmOTA3YTFkMWQwODZkODk4MDhjNmZjNjkxYmY3ZDE2OWU2ZTM4YjI0YzAxMTBjN2I5NTBjNGRkYTI3NDJkIn0.-LIGzpm0_C6U9Sj1lOr299WIMyyUEyuxWpEcV-QmU1E',
    extension: 'bsp/pantavisor',
    isExpandable: false,
    hidden: false,
    value: '405f907a1d1d086d89808c6fc691bf7d169e6e38b24c0110c7b950c4dda2742d'
  },
  'bsp/run.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      addons: [
        'addon-plymouth.cpio.xz4'
      ],
      firmware: 'firmware.squashfs',
      initrd: 'pantavisor',
      initrd_config: '',
      linux: 'kernel.img',
      modules: 'modules.squashfs'
    }
  },
  'bsp/src.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      '#spec': 'bsp-manifest-src@1',
      pvr: 'https://pvr.pantahub.com/pantahub-ci/arm_rpi64_bsp_latest#bsp'
    }
  },
  'network-mapping.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {}
  },
  'pv-avahi/lxc.container.conf': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjZmFiODI1NDI4NmMxYWFkNWExYTdkM2IwMDFlYmM0NWM5NmRlNWY4YzA5NzE3YTQwNjNmN2MxOGMyNDEwOTk2IiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoicHYtYXZhaGkvbHhjLmNvbnRhaW5lci5jb25mIiwiU2l6ZSI6NTAwLCJNZXRob2QiOiJHRVQiLCJTaGEiOiIwM2M3YTAwOTI1OTQ3NzcyMWY2NWMxMzIwMmMyMDZiOTYwNDZhNzY4ZjcxMjgyOWY2ZGRlMmZhZTUzNTVmNzViIn0.kxYzutHbavj4YJBSMe7DJsQXj7gjt2bV5JFTpc5tp5I',
    extension: 'conf',
    isExpandable: false,
    hidden: false,
    value: '03c7a009259477721f65c13202c206b96046a768f712829f6dde2fae5355f75b'
  },
  'pv-avahi/root.squashfs': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiIzNWQwMDRkMjVlNzU4MzBlYjBiOWRhMTBmMGJkYTJmYjY4MWJmYzVmMTYxODg2NzVjYTE3MGUxYzU3Nzc5YmQzIiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoicHYtYXZhaGkvcm9vdC5zcXVhc2hmcyIsIlNpemUiOjQ3MTg1OTIsIk1ldGhvZCI6IkdFVCIsIlNoYSI6IjdmNjRhYjlhZjkzYTNjNDA4YmEyZDUyYmQxZWEzNWZiOGRmMmNjMGY2YTM1NDA1YmE1YjM3MjM2ZmZlNWJmMjQifQ.WrZchZ6-c2LCoZz6NJLZTxdERJUBQ1kEAlMZupm9YSo',
    extension: 'squashfs',
    isExpandable: false,
    hidden: false,
    value: '7f64ab9af93a3c408ba2d52bd1ea35fb8df2cc0f6a35405ba5b37236ffe5bf24'
  },
  'pv-avahi/root.squashfs.docker-digest': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJmMzQzNjBkMTNjODJjODBlZmI2OTY5MzZjMDllM2FiMzAxYmQwNGY1ODVmYTI1NDMwNzI3MGE4NjY0YzU1MzhlIiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoicHYtYXZhaGkvcm9vdC5zcXVhc2hmcy5kb2NrZXItZGlnZXN0IiwiU2l6ZSI6MTIyLCJNZXRob2QiOiJHRVQiLCJTaGEiOiI1Nzk4YzY4OWU0NDVmN2Q1MTMyYWY5MDdiZGJhMDgyZjAyMTFjODJiZTVkY2NlYjRkZmVkM2M1ZGIzMWYyYjY0In0.bb_VjZ6-LN-mXdw0KNADg6wRO51kWDOIUd2MVVwRZp8',
    extension: 'docker-digest',
    isExpandable: false,
    hidden: false,
    value: '5798c689e445f7d5132af907bdba082f0211c82be5dcceb4dfed3c5db31f2b64'
  },
  'pv-avahi/run.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      '#spec': 'service-manifest-run@1',
      config: 'lxc.container.conf',
      name: 'pv-avahi',
      'root-volume': 'root.squashfs',
      storage: {
        'lxc-overlay': {
          persistence: 'boot'
        }
      },
      type: 'lxc',
      volumes: []
    }
  },
  'pv-avahi/src.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      '#spec': 'service-manifest-src@1',
      args: {},
      config: {},
      docker_digest: 'registry.gitlab.com/pantacor/pv-platforms/pv-avahi@sha256:895b2af2b5d407235f2b8c7c568532108a44946898694282340ef0315e2afb28',
      docker_name: 'registry.gitlab.com/pantacor/pv-platforms/pv-avahi',
      docker_source: 'remote,local',
      docker_tag: 'arm32v6',
      persistence: {},
      template: 'builtin-lxc-docker'
    }
  },
  'pvr-sdk/lxc.container.conf': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiIxMzhiZmU0NjA4ZjEwMzIxYWYzM2I4MGExOTdkZmUyOTBiNmFhZWEzOTk1MjcwNTJiMzVlMjVhOTRmZmM4MTc2IiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoicHZyLXNkay9seGMuY29udGFpbmVyLmNvbmYiLCJTaXplIjoxMjA0LCJNZXRob2QiOiJHRVQiLCJTaGEiOiJhNjkyMDU5MTRkZTJlOGI5NTI3MGY5NDU5MWQ1YzAxNTc5NjU5MGRhNTI3M2RiMzVlZjZiMmVkNDA2MzFmY2NhIn0.WAL8kw7BFjpG9MPSH9LYNehAVvSboP0F5eCrGx_STbQ',
    extension: 'conf',
    isExpandable: false,
    hidden: false,
    value: 'a69205914de2e8b95270f94591d5c015796590da5273db35ef6b2ed40631fcca'
  },
  'pvr-sdk/root.squashfs': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJjYmU2ZTIyYTY4NDdmY2YyMDQ2ZTEzZjZjYTFhZjZjNDQ5MmJkN2FkNDM2YTNkM2Y4NmVhYzJhY2MzMWNlMzNlIiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoicHZyLXNkay9yb290LnNxdWFzaGZzIiwiU2l6ZSI6MjYzNDk1NjgsIk1ldGhvZCI6IkdFVCIsIlNoYSI6Ijg5NmQ2NjE2Njc5NGFhMTFmMTRmZmUzYzhmZGRjODBhMDU2M2Q4NWJhZTA4NmQyNWRjZTY5ODM2ZDhlYjA0NjgifQ.dDcbkSr5g9uXyzhavRwk-TCybUrtTEIjb_THr_cFyKI',
    extension: 'squashfs',
    isExpandable: false,
    hidden: false,
    value: '896d66166794aa11f14ffe3c8fddc80a0563d85bae086d25dce69836d8eb0468'
  },
  'pvr-sdk/root.squashfs.docker-digest': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJlN2E3NWE4NWQ4NjlhYWI5NmNmY2RmMjYwNTY0MGE2MWQyYjJkNGUyNTgwZTIyNDliMTEzNDk0Yjk4M2I0M2U4IiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoicHZyLXNkay9yb290LnNxdWFzaGZzLmRvY2tlci1kaWdlc3QiLCJTaXplIjoxMjEsIk1ldGhvZCI6IkdFVCIsIlNoYSI6IjM1ZTFkNTE4MGU5NTQ0NWE3ZWZmZGQ0NGU4ZWYwOTQwNWUyMDUxZDZjNTBjNmI3MTk2NTQ2OWZhYmM3NjgzNjgifQ.OwdjAth2tJcKla6768dDwxjXH2pkH6KEnYtoaCJxKOU',
    extension: 'docker-digest',
    isExpandable: false,
    hidden: false,
    value: '35e1d5180e95445a7effdd44e8ef09405e2051d6c50c6b71965469fabc768368'
  },
  'pvr-sdk/run.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      '#spec': 'service-manifest-run@1',
      config: 'lxc.container.conf',
      name: 'pvr-sdk',
      'root-volume': 'root.squashfs',
      storage: {
        'docker--etc-dropbear': {
          persistence: 'permanent'
        },
        'docker--etc-volume': {
          persistence: 'permanent'
        },
        'docker--home-pantavisor-.ssh': {
          persistence: 'permanent'
        },
        'docker--var-pvr-sdk': {
          persistence: 'permanent'
        },
        'lxc-overlay': {
          persistence: 'boot'
        }
      },
      type: 'lxc',
      volumes: []
    }
  },
  'pvr-sdk/src.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      '#spec': 'service-manifest-src@1',
      args: {
        PV_LXC_EXTRA_CONF: 'lxc.mount.entry = /volumes/_pv/addons/plymouth/text-io var/run/plymouth-io-sockets none bind,rw,optional,create=dir 0 0',
        PV_SECURITY_WITH_STORAGE: 'yes'
      },
      config: {},
      docker_digest: 'registry.gitlab.com/pantacor/pv-platforms/pvr-sdk@sha256:95b5d63a216773af9c8b2b82e25e2e9d40a049bae9c62cd2308eff5feb9b32dd',
      docker_name: 'registry.gitlab.com/pantacor/pv-platforms/pvr-sdk',
      docker_source: 'remote,local',
      docker_tag: 'arm32v6',
      persistence: {},
      template: 'builtin-lxc-docker'
    }
  },
  'storage-mapping.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {}
  },
  'webdemo/lxc.container.conf': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiIwZWI4MTM5MTk4OTNiMDc2YWU5NmVlZGI5NzdmYTU4MjEyZjBkZGVkZGY3MWFhZTMwYjFiZDBmNDdkMzc5MDdlIiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoid2ViZGVtby9seGMuY29udGFpbmVyLmNvbmYiLCJTaXplIjo1MzQsIk1ldGhvZCI6IkdFVCIsIlNoYSI6IjRiZjJjMDYyOThlZDlhOTMzOTI5ZWVkYWNkMTY2ZWI2ZmQ3YjlmOWRhMzc4NjUyNzY2NzNiYTIxNzgzNjcxMjAifQ.GdGfUX-6ZtF-RWU3OjIyBoqApb_Xomv1mBEfSSfb240',
    extension: 'conf',
    isExpandable: false,
    hidden: false,
    value: '4bf2c06298ed9a933929eedacd166eb6fd7b9f9da37865276673ba2178367120'
  },
  'webdemo/root.squashfs': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiI0MWJjNDllODg0MDIwZjFhZmM0NzhlZmJlM2U0NmE3ZmQ0Y2U4MGE1NjBlYTVmZDBlODYwNTgwZTk0Y2QzOGNhIiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoid2ViZGVtby9yb290LnNxdWFzaGZzIiwiU2l6ZSI6MzUyMjU2MCwiTWV0aG9kIjoiR0VUIiwiU2hhIjoiYjYzZTI1N2Y5YzhhZTZkNjVhYTYwNTIwMTFjYTA2MzdiNDM2ZGJlNjgyZjZlZTQyNTQ2YjY3ZDZmNDI4NGQwNSJ9.XcUESNTTHwU8os3fIY77j6DnOKL11-I6coT8WFgWr6c',
    extension: 'squashfs',
    isExpandable: false,
    hidden: false,
    value: 'b63e257f9c8ae6d65aa6052011ca0637b436dbe682f6ee42546b67d6f4284d05'
  },
  'webdemo/root.squashfs.docker-digest': {
    url: 'https://api.pantahub.com/local-s3/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJmMzlkMjZlYWI3MjNmOTZjOTI5MmZmZWJhYzc4MWE5MTYyMWNkMDVhYzVhOTE5OGJhMzg4ZDAzMjI1YTIxMDM1IiwiZXhwIjoxNjMxOTA0MzM2LCJpYXQiOjE2MzE4MTc5MzYsImlzcyI6Imh0dHBzOi8vYXBpLnBhbnRhaHViLmNvbS90cmFpbHMiLCJzdWIiOiJwcm46OjphY2NvdW50czovNWM4ZjkzZGNlZWE4MjMwMDA4NzZjNGZhIiwiRGlzcG9zaXRpb25OYW1lIjoid2ViZGVtby9yb290LnNxdWFzaGZzLmRvY2tlci1kaWdlc3QiLCJTaXplIjo5MSwiTWV0aG9kIjoiR0VUIiwiU2hhIjoiYWFjZGE0M2ViZjEyYTA1Y2VmZGI0NWMzMDg0MjM2OGM1MTE3OGJiNDEzZjc3NDFmMGE0NmFkYmVlNTY2NTNlOSJ9.OmyHnKD6eQoQuHCJlegZJyBXwEz0M69D6EzN7cVi1g8',
    extension: 'docker-digest',
    isExpandable: false,
    hidden: false,
    value: 'aacda43ebf12a05cefdb45c30842368c51178bb413f7741f0a46adbee56653e9'
  },
  'webdemo/run.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      '#spec': 'service-manifest-run@1',
      config: 'lxc.container.conf',
      name: 'webdemo',
      'root-volume': 'root.squashfs',
      runlevel: 'app',
      storage: {
        'lxc-overlay': {
          persistence: 'boot'
        }
      },
      type: 'lxc',
      volumes: []
    }
  },
  'webdemo/src.json': {
    url: null,
    extension: 'json',
    isExpandable: true,
    hidden: false,
    value: {
      '#spec': 'service-manifest-src@1',
      args: {
        PV_RUNLEVEL: 'app'
      },
      config: {},
      docker_digest: 'highercomve/webdemo@sha256:947baae1c682b00cfc275c6d75fa9946ce801ecae42bf2608b89ab0a3c0839b6',
      docker_name: 'highercomve/webdemo',
      docker_platform: 'linux/arm',
      docker_source: 'remote,local',
      docker_tag: 'latest',
      persistence: {},
      template: 'builtin-lxc-docker'
    }
  }
}
