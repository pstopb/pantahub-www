export const DeviceState = {
  '#spec': 'pantavisor-service-system@1',
  '_config/awconnect/etc/NetworkManager/dnsmasq-shared.d/device-dns.conf': '4a3d315468684cd64ef6915774a867121558297ca9fbab2ed9dd985eb7343886',
  '_config/awconnect/etc/iptables/dump': 'cf81ec015d8fc3f7caf1d13db7785405c7ff0167aec0cb5dd2d75d95fc67b54a',
  '_config/awconnect/etc/iptables/rules.v4': '50dc17c1fc5c128a9f69faf15983323a4abee4dfc2f1dd99fb956ab03171e995',
  '_config/awconnect/etc/iptables/rules.v6': 'deb83d92c585315d85d7316c1d76064a87b76ab580c8c21af49aa2f6a7f96fe9',
  '_config/awconnect/etc/wifi-connect/conf.d/WifiHotspot.json': {
    psk: 'p4nt4c0r',
    ssid: 'pantacor'
  },
  '_config/bitwardenrs/etc/ngrok/config.json': {
    authtoken: '6oopC3D5JzNydEARtihhB_9mFZ6843hCxixfcThT1n'
  },
  '_config/openvpn/vpn/config.json': {
    auth: 'bEM7MnTN9j2WW-5cy9CRMhQg\nkGzPChZKSLwFK0zCSx23ptMb',
    config: '# ==============================================================================\n# Copyright (c) 2016-2020 Proton Technologies AG (Switzerland)\n# Email: contact@protonvpn.com\n#\n# The MIT License (MIT)\n#\n# Permission is hereby granted, free of charge, to any person obtaining a copy\n# of this software and associated documentation files (the \'Software\'), to deal\n# in the Software without restriction, including without limitation the rights\n# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\n# copies of the Software, and to permit persons to whom the Software is\n# furnished to do so, subject to the following conditions:\n#\n# The above copyright notice and this permission notice shall be included in all\n# copies or substantial portions of the Software.\n#\n# THE SOFTWARE IS PROVIDED \'AS IS\', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR # OTHERWISE, ARISING\n# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS\n# IN THE SOFTWARE.\n# ==============================================================================\n\nclient\ndev tun\nproto udp\n\nremote us-free-01.protonvpn.com 80\nremote us-free-01.protonvpn.com 443\nremote us-free-01.protonvpn.com 4569\nremote us-free-01.protonvpn.com 1194\nremote us-free-01.protonvpn.com 5060\n\nremote-random\nresolv-retry infinite\nnobind\ncipher AES-256-CBC\nauth SHA512\ncomp-lzo no\nverb 3\n\ntun-mtu 1500\ntun-mtu-extra 32\nmssfix 1450\npersist-key\npersist-tun\n\nreneg-sec 0\n\nremote-cert-tls server\nauth-user-pass /etc/openvpn/auth.conf\npull\nfast-io\n\n\n<ca>\n-----BEGIN CERTIFICATE-----\nMIIFozCCA4ugAwIBAgIBATANBgkqhkiG9w0BAQ0FADBAMQswCQYDVQQGEwJDSDEV\nMBMGA1UEChMMUHJvdG9uVlBOIEFHMRowGAYDVQQDExFQcm90b25WUE4gUm9vdCBD\nQTAeFw0xNzAyMTUxNDM4MDBaFw0yNzAyMTUxNDM4MDBaMEAxCzAJBgNVBAYTAkNI\nMRUwEwYDVQQKEwxQcm90b25WUE4gQUcxGjAYBgNVBAMTEVByb3RvblZQTiBSb290\nIENBMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAt+BsSsZg7+AuqTq7\nvDbPzfygtl9f8fLJqO4amsyOXlI7pquL5IsEZhpWyJIIvYybqS4s1/T7BbvHPLVE\nwlrq8A5DBIXcfuXrBbKoYkmpICGc2u1KYVGOZ9A+PH9z4Tr6OXFfXRnsbZToie8t\n2Xjv/dZDdUDAqeW89I/mXg3k5x08m2nfGCQDm4gCanN1r5MT7ge56z0MkY3FFGCO\nqRwspIEUzu1ZqGSTkG1eQiOYIrdOF5cc7n2APyvBIcfvp/W3cpTOEmEBJ7/14RnX\nnHo0fcx61Inx/6ZxzKkW8BMdGGQF3tF6u2M0FjVN0lLH9S0ul1TgoOS56yEJ34hr\nJSRTqHuar3t/xdCbKFZjyXFZFNsXVvgJu34CNLrHHTGJj9jiUfFnxWQYMo9UNUd4\na3PPG1HnbG7LAjlvj5JlJ5aqO5gshdnqb9uIQeR2CdzcCJgklwRGCyDT1pm7eoiv\nWV19YBd81vKulLzgPavu3kRRe83yl29It2hwQ9FMs5w6ZV/X6ciTKo3etkX9nBD9\nZzJPsGQsBUy7CzO1jK4W01+u3ItmQS+1s4xtcFxdFY8o/q1zoqBlxpe5MQIWN6Qa\nlryiET74gMHE/S5WrPlsq/gehxsdgc6GDUXG4dk8vn6OUMa6wb5wRO3VXGEc67IY\nm4mDFTYiPvLaFOxtndlUWuCruKcCAwEAAaOBpzCBpDAMBgNVHRMEBTADAQH/MB0G\nA1UdDgQWBBSDkIaYhLVZTwyLNTetNB2qV0gkVDBoBgNVHSMEYTBfgBSDkIaYhLVZ\nTwyLNTetNB2qV0gkVKFEpEIwQDELMAkGA1UEBhMCQ0gxFTATBgNVBAoTDFByb3Rv\nblZQTiBBRzEaMBgGA1UEAxMRUHJvdG9uVlBOIFJvb3QgQ0GCAQEwCwYDVR0PBAQD\nAgEGMA0GCSqGSIb3DQEBDQUAA4ICAQCYr7LpvnfZXBCxVIVc2ea1fjxQ6vkTj0zM\nhtFs3qfeXpMRf+g1NAh4vv1UIwLsczilMt87SjpJ25pZPyS3O+/VlI9ceZMvtGXd\nMGfXhTDp//zRoL1cbzSHee9tQlmEm1tKFxB0wfWd/inGRjZxpJCTQh8oc7CTziHZ\nufS+Jkfpc4Rasr31fl7mHhJahF1j/ka/OOWmFbiHBNjzmNWPQInJm+0ygFqij5qs\n51OEvubR8yh5Mdq4TNuWhFuTxpqoJ87VKaSOx/Aefca44Etwcj4gHb7LThidw/ky\nzysZiWjyrbfX/31RX7QanKiMk2RDtgZaWi/lMfsl5O+6E2lJ1vo4xv9pW8225B5X\neAeXHCfjV/vrrCFqeCprNF6a3Tn/LX6VNy3jbeC+167QagBOaoDA01XPOx7Odhsb\nGd7cJ5VkgyycZgLnT9zrChgwjx59JQosFEG1DsaAgHfpEl/N3YPJh68N7fwN41Cj\nzsk39v6iZdfuet/sP7oiP5/gLmA/CIPNhdIYxaojbLjFPkftVjVPn49RqwqzJJPR\nN8BOyb94yhQ7KO4F3IcLT/y/dsWitY0ZH4lCnAVV/v2YjWAWS3OWyC8BFx/Jmc3W\nDK/yPwECUcPgHIeXiRjHnJt0Zcm23O2Q3RphpU+1SO3XixsXpOVOYP6rJIXW9bMZ\nA1gTTlpi7A==\n-----END CERTIFICATE-----\n-----BEGIN CERTIFICATE-----\nMIIFszCCA5ugAwIBAgIBBjANBgkqhkiG9w0BAQ0FADBAMQswCQYDVQQGEwJDSDEV\nMBMGA1UEChMMUHJvdG9uVlBOIEFHMRowGAYDVQQDExFQcm90b25WUE4gUm9vdCBD\nQTAeFw0xNzAyMTUxNTE3MDBaFw0yNzAyMTUxNDM4MDBaMEoxCzAJBgNVBAYTAkNI\nMRUwEwYDVQQKEwxQcm90b25WUE4gQUcxJDAiBgNVBAMTG1Byb3RvblZQTiBJbnRl\ncm1lZGlhdGUgQ0EgMTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBANv3\nuwQMFjYOx74taxadhczLbjCTuT73jMz09EqFNv7O7UesXfYJ6kQgYV9YyE86znP4\nxbsswNUZYh+XdZUpOoP6Zu3tR/iiYiuzi6jVYrJ66G89nPqS2mm5dn8Fbb8CRWkJ\nygm8AdlYkDwYNldhDUrERlQdCRDGsYYg/98dded+5pXnSG8Y/+iuLM6/YYhkUVQe\nCfq1L6XguSwu8CuvJjIjjE1PptUHa3Hc3tGziVydltKynxWlqb1dJqinGKiBZvYn\noiV4motpFYwhc3Wd09JLPzeobhD2IAZ2evSatikMWDingEv1EJXpI+V/E2AK3xHK\nSkhw+YZx99tNxCiOu3U5BFAreZR3j2YnZzX1nEv9p02IGaWzzYJPNED0zSO2w07u\nthSmKcxA39VTvs91lptbcV7VTxoJY0SErHIeVS3Scrnr7WvoOTuu3M3SCRqe6oI9\noJZMOdfNsceBdvG+qlpOFICoBjO53W4BK8KahzTd/PWlBRiVJ3UVv8xXwUDA+o98\n34DXVAobaAHXQtM9jNobqT98FXhZktjOQEA2UORL581ZPxfKeHLRcgWJ5dmPsDBG\ny/L6/qW/yrm6DUDAdN5+q41+gSNEjNBjLBJQFUmDk3l6Qxiu0uEDQ98oFvGHk5US\n2Kbj0OAq1RpiDjHci/536yua9rTC+cxekTM2asdXAgMBAAGjga0wgaowEgYDVR0T\nAQH/BAgwBgEB/wIBADAdBgNVHQ4EFgQUJbaTWcIB4t5ETvvhUy5/yQqqGjMwaAYD\nVR0jBGEwX4AUg5CGmIS1WU8MizU3rTQdqldIJFShRKRCMEAxCzAJBgNVBAYTAkNI\nMRUwEwYDVQQKEwxQcm90b25WUE4gQUcxGjAYBgNVBAMTEVByb3RvblZQTiBSb290\nIENBggEBMAsGA1UdDwQEAwIBBjANBgkqhkiG9w0BAQ0FAAOCAgEAAgZ/BIVl+DcK\nOTVJJBy+RZ1E8os11gFaMKy12lAT1XEXDqLAnitvVyQgG5lPZKFQ2wzUR/TCrYKT\nSUZWdYaJIXkRWAU0aCDZ2I81T0OMpg9aS7xdxgHCGWOwwes8GhjtvQad9GJ8mUZH\nGyzfMaGG6fAZrgHnlOb4OIoqhBWYla6D2bpvbKgGkMo5NLAaX/7+U0HcxjjSS9vm\n/3XHTZU4q77pn+lhPWncajnVyMtm1mIZxMioyckR4+scyZse0mYJS6xli/7crH7j\nqScX7c5sWcaN4J63a3+x3uGvzOXjCyoDl9IaeqnxQpi8yc0nsWxIyDalR3uRQ9tJ\n7l/eRxJZ/1Pzz2LRHSQZuqN2ZReWVNTqJ42af8cWWH0fDOEt2468GLeSm08Hvyz0\nlRjn7Tf5hxOJSw4/3oGihvzuTdquJMOi62kThbp7DS3mMaZsfbmDoU3oNDv91bvL\n57z8wm7yRcGEoMsUNnrOZ4SU8dG/souvJM1BDStMLprFEgUbHEY5MjSR4/PLR6j9\n3NZgocfnfk80nBvNtgWVHxW019nuT93WL0/5L5g4UVm0Ay1V6pNkGZCmgNUBaRY4\n2JLzyY8p48OKapR5GnedLTJXJVbdd9GUNzIzm4iVITDH3p/u1g69dITCNXTO9EO5\nsGEYLNPbV49XBnVAm1tUWuoByZAjoWs=\n-----END CERTIFICATE-----\n</ca>\n\nkey-direction 1\n<tls-auth>\n# 2048 bit OpenVPN static key\n-----BEGIN OpenVPN Static key V1-----\n6acef03f62675b4b1bbd03e53b187727\n423cea742242106cb2916a8a4c829756\n3d22c7e5cef430b1103c6f66eb1fc5b3\n75a672f158e2e2e936c3faa48b035a6d\ne17beaac23b5f03b10b868d53d03521d\n8ba115059da777a60cbfd7b2c9c57472\n78a15b8f6e68a3ef7fd583ec9f398c8b\nd4735dab40cbd1e3c62a822e97489186\nc30a0b48c7c38ea32ceb056d3fa5a710\ne10ccc7a0ddb363b08c3d2777a3395e1\n0c0b6080f56309192ab5aacd4b45f55d\na61fc77af39bd81a19218a79762c3386\n2df55785075f37d8c71dc8a42097ee43\n344739a0dd48d03025b0450cf1fb5e8c\naeb893d9a96d1f15519bb3c4dcb40ee3\n16672ea16c012664f8a9f11255518deb\n-----END OpenVPN Static key V1-----\n</tls-auth>'
  },
  '_config/pv-avahi/etc/hostname': '2c8b08da5ce60398e1f19af0e5dccc744df274b826abe585eaba68c525434806',
  '_config/tailscale/etc/tailscale/config.json': {
    acceptdns: true,
    acceptroutes: true,
    hostname: 'one_router_rpi3_002',
    hostroutes: true,
    key: 'tskey-afc07ea50db2799d1965017e',
    shieldsup: false,
    snatsubnetroutes: true
  },
  '_hostconfig/pvr/docker.json': {
    platforms: [
      'linux/arm64',
      'linux/arm'
    ]
  },
  'adguard/_awall/dnscrypt.json': {
    description: 'Forward DNSEncrypt to this container',
    filter: [
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'internet',
        service: 'http'
      },
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'internet',
        service: 'adguard-dnsencrypt'
      },
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'intranet',
        service: 'http'
      },
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'intranet',
        service: 'adguard-dnsencrypt'
      }
    ],
    service: {
      'adguard-dnsencrypt': [
        {
          port: 5443,
          proto: 'tcp'
        },
        {
          port: 8853,
          proto: 'udp'
        }
      ]
    }
  },
  'adguard/_awall/dnsovertls.json': {
    description: 'Forward dns over tls to this container',
    filter: [
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'internet',
        service: 'http'
      },
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'internet',
        service: 'adguard-dotls'
      },
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'intranet',
        service: 'http'
      },
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'intranet',
        service: 'adguard-dotls'
      }
    ],
    service: {
      'adguard-dotls': [
        {
          port: 53,
          proto: 'tcp'
        },
        {
          port: 53,
          proto: 'udp'
        },
        {
          port: 853,
          proto: 'tcp'
        },
        {
          port: 853,
          proto: 'udp'
        },
        {
          port: 784,
          proto: 'udp'
        },
        {
          port: 8853,
          proto: 'udp'
        },
        {
          port: 5443,
          proto: 'tcp'
        },
        {
          port: 8853,
          proto: 'udp'
        }
      ]
    }
  },
  'adguard/_awall/http.json': {
    description: 'Forward HTTP to this container',
    filter: [
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'internet',
        service: 'http'
      },
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'internet',
        service: 'adguard-web'
      },
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'intranet',
        service: 'http'
      },
      {
        action: 'accept',
        'conn-limit': {
          count: 100,
          interval: 20
        },
        dnat: '10.0.3.20',
        in: 'intranet',
        service: 'adguard-web'
      }
    ],
    service: {
      'adguard-web': [
        {
          port: 3000,
          proto: 'tcp'
        },
        {
          port: 3000,
          proto: 'udp'
        }
      ]
    }
  },
  'adguard/lxc.container.conf': '56988ed49e7c24e6279635eed53584b60160100f530b456f813fc75a1b83c44f',
  'adguard/root.squashfs': '788492861f367de55e411edc561b54230b325516d3b345e2f0f29485eaf9ffec',
  'adguard/root.squashfs.docker-digest': 'a1a073d11753ce19dc08ce4766a3363b372db664e2fdd4a6b9f75faee6fa61d5',
  'adguard/run.json': {
    '#spec': 'service-manifest-run@1',
    config: 'lxc.container.conf',
    name: 'adguard',
    'root-volume': 'root.squashfs',
    runlevel: 'app',
    storage: {
      'docker--opt-adguardhome-conf': {
        persistence: 'permanent'
      },
      'docker--opt-adguardhome-work': {
        persistence: 'permanent'
      },
      'lxc-overlay': {
        persistence: 'boot'
      }
    },
    type: 'lxc',
    volumes: []
  },
  'adguard/src.json': {
    '#spec': 'service-manifest-src@1',
    args: {
      PV_LXC_NETWORK_IPV4_ADDRESS: '10.0.3.20/24',
      PV_LXC_NETWORK_TYPE: 'veth',
      PV_RUNLEVEL: 'app'
    },
    config: {},
    docker_digest: 'adguard/adguardhome@sha256:cd5e6641e969ec8a1df1ed02dc969db49d6cf540055f14346d0d5d42951f75d6',
    docker_name: 'adguard/adguardhome',
    docker_platform: 'linux/arm',
    docker_source: 'remote,local',
    docker_tag: 'latest',
    persistence: {
      '/opt/adguardhome/conf/': 'permanent',
      '/opt/adguardhome/work/': 'permanent'
    },
    template: 'builtin-lxc-docker'
  },
  'awall.json': {
    description: 'How to use awall',
    filter: [
      {
        action: 'accept',
        in: 'vpn',
        service: [
          'ping',
          'dns',
          'dhcp',
          'ssh',
          'pvssh',
          'bitwarden'
        ]
      },
      {
        action: 'accept',
        in: 'internet',
        service: [
          'ping',
          'dns',
          'dhcp',
          'ssh',
          'pvssh',
          'bitwarden',
          'openvpn'
        ]
      },
      {
        action: 'accept',
        in: 'intranet',
        service: [
          'dns',
          'dhcp',
          'ping',
          'ssh',
          'pvssh',
          'bitwarden'
        ]
      },
      {
        action: 'accept',
        in: 'internet',
        out: '_fw',
        service: [
          'ssh',
          'pvssh'
        ]
      },
      {
        action: 'accept',
        out: 'internet',
        service: [
          'http',
          'https',
          'openvpn'
        ]
      }
    ],
    import: [
      'bsp'
    ],
    policy: [
      {
        action: 'accept',
        out: 'internet'
      },
      {
        action: 'drop',
        in: 'internet'
      },
      {
        action: 'accept',
        out: 'vpn'
      },
      {
        action: 'drop',
        in: 'vpn'
      },
      {
        action: 'accept',
        out: 'intranet'
      },
      {
        action: 'drop',
        in: 'intranet'
      },
      {
        action: 'accept',
        in: 'containernet'
      },
      {
        action: 'accept',
        out: 'containernet'
      },
      {
        action: 'reject'
      }
    ],
    service: {
      bitwarden: [
        {
          port: 8642,
          proto: 'tcp'
        },
        {
          port: 8642,
          proto: 'udp'
        }
      ],
      openvpn: [
        {
          port: 4569,
          proto: 'udp'
        },
        {
          port: 5060,
          proto: 'udp'
        },
        {
          port: 1194,
          proto: 'udp'
        }
      ],
      pvssh: [
        {
          port: 8222,
          proto: 'tcp'
        }
      ]
    },
    snat: [
      {
        out: 'internet'
      },
      {
        out: 'vpn'
      }
    ],
    zone: {
      containernet: {
        iface: '$containernet_if'
      },
      internet: {
        iface: '$wan_if'
      },
      intranet: {
        iface: '$lan_if'
      },
      vpn: {
        iface: '$vpn_if'
      }
    }
  },
  awall2pvmwall: '735d94c0ce63756654e8ad351b7f913ca19e2c872aed2186ddf8d2e00d05313c',
  'awconnect/lxc.container.conf': '153d58588b0327f73c8424c214c039fcdd975814bc075bc5c72f82fd3cdfd7b6',
  'awconnect/root.squashfs': '72d4291c79f44b032cde32764da17006e2f0eb3f9ca375ca6ddb04e1fd59ba71',
  'awconnect/root.squashfs.docker-digest': '047def69f85dbf042542795834e1ade15e9a4b7e775526bed343ce4f177a662c',
  'awconnect/run.json': {
    '#spec': 'service-manifest-run@1',
    config: 'lxc.container.conf',
    name: 'awconnect',
    'root-volume': 'root.squashfs',
    storage: {
      'docker--etc-NetworkManager-system-connections': {
        persistence: 'permanent'
      },
      'lxc-overlay': {
        persistence: 'boot'
      }
    },
    type: 'lxc',
    volumes: []
  },
  'awconnect/src.json': {
    '#spec': 'service-manifest-src@1',
    args: {},
    config: {},
    docker_digest: 'registry.gitlab.com/pantacor/pv-platforms/wifi-connect@sha256:5c889720c6243408049b7a2f9ec75b9f4e376f6cbc28e39e091420a1e19df2aa',
    docker_name: 'registry.gitlab.com/pantacor/pv-platforms/wifi-connect',
    docker_source: 'remote,local',
    docker_tag: 'arm32v5',
    persistence: {},
    template: 'builtin-lxc-docker'
  },
  'bitwardenrs/lxc.container.conf': '56f9ed597f2af96efc6b91ebbfa81dc208b83dfaf0d583ac3797b050f8324833',
  'bitwardenrs/root.squashfs': 'c0d1c46c9e9e5945dc37a3707b53fa9399fdbce6dcbb98e1b7a65ded46e6b7b0',
  'bitwardenrs/root.squashfs.docker-digest': '27984b47bd9c566bca1cb0a50c753fc2e2cd035b190609f04b6ca14e771c7112',
  'bitwardenrs/run.json': {
    '#spec': 'service-manifest-run@1',
    config: 'lxc.container.conf',
    name: 'bitwardenrs',
    'root-volume': 'root.squashfs',
    runlevel: 'app',
    storage: {
      'docker--data': {
        persistence: 'permanent'
      },
      'lxc-overlay': {
        persistence: 'boot'
      }
    },
    type: 'lxc',
    volumes: []
  },
  'bitwardenrs/src.json': {
    '#spec': 'service-manifest-src@1',
    args: {
      PV_RUNLEVEL: 'app'
    },
    config: {},
    docker_digest: 'bitwardenrs/server@sha256:ef129de113bec3409b6370c37a6e5573a1dacc051a3aae2a8a3339323ae63623',
    docker_name: 'bitwardenrs/server@sha256',
    docker_source: 'remote,local',
    docker_tag: 'ef129de113bec3409b6370c37a6e5573a1dacc051a3aae2a8a3339323ae63623',
    persistence: {},
    template: 'builtin-lxc-docker'
  },
  'bitwardenrs/v0alpha.app.manifest.json': {
    '#spec': 'app-manifest@1',
    _configfiles: [
      {
        file: '/etc/ngrok/config.json',
        name: 'ngrok.config',
        schema: {
          description: 'Configure your ngrok tunnel',
          properties: {
            authtoken: {
              title: 'Authorization Token',
              type: 'string'
            }
          },
          required: [
            'authtoken'
          ],
          title: 'ngrok configuration',
          type: 'object'
        },
        type: 'jsonschema',
        ui: {
          authtoken: {
            'ui:autofocus': true
          }
        }
      }
    ],
    _metaconfig: [],
    _readme: 'Configure your ngrok credentials to make Bitwarden available to the Internet.',
    _status: [
      {
        properties: [
          {
            default: 'Not connected',
            key: 'device.id',
            title: 'Device ID',
            type: 'device'
          },
          {
            default: 'http://ngrok.io',
            key: 'ngrok.url',
            title: 'ngrok url',
            type: 'device-meta'
          }
        ],
        title: 'Bitwarden configuration'
      }
    ],
    description: 'Bitwarden offers the easiest and safest way for teams and individuals to store and share sensitive data from any device.',
    icon: 'cloud',
    logo: 'https://raw.githubusercontent.com/bitwarden/brand/master/logos/bitwarden-shield-padded.png',
    title: 'Bitwarden',
    version: '0.0.1 '
  },
  'bsp/_awall.json': {
    variable: {
      containernet_if: 'lxcbr0',
      lan_if: 'wlan0',
      vpn_if: 'tailscale0',
      wan_if: 'enxb827eb620e63'
    }
  },
  'bsp/addon-plymouth.cpio.xz4': 'beae6a7bb235916cac52bcfece64c30615cded8c4c640e6941e7ecabe53b4920',
  'bsp/build.json': {
    altrepogroups: '',
    branch: 'master',
    commit: '8da5ae49e8881cb19c7809d2741871b6ed356976',
    gitdescribe: '014-rc8-31-g8da5ae4',
    pipeline: '346849701',
    platform: 'rpi64',
    project: 'pantacor/pv-manifest',
    pvrversion: 'pvr version 022-11-g0399aa1d',
    target: 'arm-rpi64',
    time: '2021-08-03 00:26:03 +0000'
  },
  'bsp/firmware.squashfs': 'bc11c1054aa1c252811778cce9379389831b02ca01ce78eba54a99987d9867d4',
  'bsp/kernel.img': '5e70b9e6a3bac6e4f00ac04871b91bbf4bdb6c6efbb1ba0aef24a28a2ed9ac63',
  'bsp/modules.squashfs': '4e102f1827a58fd0281a219bfc6bd187d7e07080fce69f40b853bca4c29de71b',
  'bsp/pantavisor': '05e6ad3526acfe876c651b9499b5d79144b36a1f9b2f6cde216223583a6dab27',
  'bsp/run.json': {
    addons: [
      'addon-plymouth.cpio.xz4'
    ],
    firmware: 'firmware.squashfs',
    initrd: 'pantavisor',
    initrd_config: '',
    linux: 'kernel.img',
    modules: 'modules.squashfs'
  },
  'bsp/src.json': {
    '#spec': 'bsp-manifest-src@1',
    pvr: 'https://pvr.pantahub.com/pantahub-ci/arm_rpi64_bsp_latest#bsp'
  },
  'network-mapping.json': {},
  'openvpn/lxc.container.conf': 'bd772678adfcfca540fea1ea8b4b99a272eb3033d2e0ecf615ead61a50caf9c6',
  'openvpn/root.squashfs': 'afd93dcf18919d16953713e2518afc5a0f7244dddea2f5f731862597102f712a',
  'openvpn/root.squashfs.docker-digest': 'a8cb15cd0c73ab9266816c7ec054865da114e6133436acf623d4da33c37d3db4',
  'openvpn/run.json': {
    '#spec': 'service-manifest-run@1',
    config: 'lxc.container.conf',
    name: 'openvpn',
    'root-volume': 'root.squashfs',
    storage: {
      'lxc-overlay': {
        persistence: 'boot'
      }
    },
    type: 'lxc',
    volumes: []
  },
  'openvpn/src.json': {
    '#spec': 'service-manifest-src@1',
    args: {
      PV_LXC_EXTRA_CONF: 'lxc.cgroup.devices.allow = c 10:200 rwm',
      PV_RUNLEVEL: 'app'
    },
    config: {},
    docker_digest: 'sha256:f27978b5ccb82d08514c87445612e872c36860f95bd0b471456887212d69706e',
    docker_name: 'highercomve/openvpn',
    docker_source: 'remote,local',
    docker_tag: 'arm32v6',
    persistence: {},
    template: 'builtin-lxc-docker'
  },
  'openvpn/v0alpha.app.manifest.json': {
    '#spec': 'app-manifest@1',
    _configfiles: [
      {
        file: '/vpn/config.json',
        name: 'openvpn.config',
        schema: {
          description: 'Configure your OpenVPN',
          properties: {
            auth: {
              title: '/etc/openvpn/auth.config',
              type: 'string'
            },
            config: {
              title: '/etc/openvpn/config.ovpn',
              type: 'string'
            }
          },
          required: [
            'config'
          ],
          title: 'OpenVPN configuration',
          type: 'object'
        },
        type: 'jsonschema',
        ui: {
          auth: {
            'ui:autofocus': true,
            'ui:options': {
              rows: 3
            },
            'ui:widget': 'textarea'
          },
          config: {
            'ui:autofocus': true,
            'ui:options': {
              rows: 20
            },
            'ui:widget': 'textarea'
          },
          'ui:order': [
            'config',
            'auth',
            '*'
          ]
        }
      }
    ],
    _metaconfig: [
      {
        'meta-key-prefix': 'openvpn',
        name: 'openvpn.meta',
        schema: {
          description: '',
          properties: {
            enabled: {
              default: false,
              title: 'OpenVPN',
              type: 'boolean'
            }
          },
          required: [],
          title: '',
          type: 'object'
        },
        type: 'jsonschema',
        ui: {}
      }
    ],
    _status: [
      {
        properties: [
          {
            default: 'Not connected',
            key: 'interfaces.tun0.ipv4.0',
            title: 'VPN tunnel IP',
            type: 'device-meta'
          }
        ],
        title: 'Vpn connection'
      }
    ],
    description: 'OpenVPN provides flexible VPN solutions to secure your data communications, whether it\'s for Internet privacy, remote access for employees, securing IoT, or for networking Cloud data centers. Our VPN Server software solution can be deployed on-premises using standard servers or virtual appliances, or on the cloud.',
    icon: 'vpn_key',
    logo: '',
    title: 'OpenVPN',
    version: '0.0.1'
  },
  'pv-avahi/lxc.container.conf': '03c7a009259477721f65c13202c206b96046a768f712829f6dde2fae5355f75b',
  'pv-avahi/root.squashfs': '7f64ab9af93a3c408ba2d52bd1ea35fb8df2cc0f6a35405ba5b37236ffe5bf24',
  'pv-avahi/root.squashfs.docker-digest': '5798c689e445f7d5132af907bdba082f0211c82be5dcceb4dfed3c5db31f2b64',
  'pv-avahi/run.json': {
    '#spec': 'service-manifest-run@1',
    config: 'lxc.container.conf',
    name: 'pv-avahi',
    'root-volume': 'root.squashfs',
    storage: {
      'lxc-overlay': {
        persistence: 'boot'
      }
    },
    type: 'lxc',
    volumes: []
  },
  'pv-avahi/src.json': {
    '#spec': 'service-manifest-src@1',
    args: {},
    config: {},
    docker_digest: 'registry.gitlab.com/pantacor/pv-platforms/pv-avahi@sha256:895b2af2b5d407235f2b8c7c568532108a44946898694282340ef0315e2afb28',
    docker_name: 'registry.gitlab.com/pantacor/pv-platforms/pv-avahi',
    docker_source: 'remote,local',
    docker_tag: 'arm32v6',
    persistence: {},
    template: 'builtin-lxc-docker'
  },
  'pvr-sdk/lxc.container.conf': 'e493f2572274aee4bff712e1af023ff9b783df005bbc678a802bf1962d9c78a5',
  'pvr-sdk/root.squashfs': '781ba23800c2e2bd3e4f9a0367e1ba07d39372fc5ec90a112228f3a0fda0a640',
  'pvr-sdk/root.squashfs.docker-digest': '52313829e01969b6386d329bbaeb460969114004be435b402fb3b769eed36563',
  'pvr-sdk/run.json': {
    '#spec': 'service-manifest-run@1',
    config: 'lxc.container.conf',
    name: 'pvr-sdk',
    'root-volume': 'root.squashfs',
    storage: {
      'docker--etc-dropbear': {
        persistence: 'permanent'
      },
      'docker--etc-volume': {
        persistence: 'permanent'
      },
      'docker--home-pantavisor-.ssh': {
        persistence: 'permanent'
      },
      'docker--var-pvr-sdk': {
        persistence: 'permanent'
      },
      'lxc-overlay': {
        persistence: 'boot'
      }
    },
    type: 'lxc',
    volumes: []
  },
  'pvr-sdk/src.json': {
    '#spec': 'service-manifest-src@1',
    args: {
      PV_LXC_EXTRA_CONF: 'lxc.mount.entry = /volumes/_pv/addons/plymouth/text-io var/run/plymouth-io-sockets none bind,rw,optional,create=dir 0 0',
      PV_SECURITY_WITH_STORAGE: 'yes'
    },
    config: {},
    docker_digest: 'registry.gitlab.com/pantacor/pv-platforms/pvr-sdk@sha256:12e289fc5a1de14591759153d912f2d1945de41684440113370a5cc054f6b5cb',
    docker_name: 'registry.gitlab.com/pantacor/pv-platforms/pvr-sdk',
    docker_source: 'remote,local',
    docker_tag: 'arm32v6',
    persistence: {},
    template: 'builtin-lxc-docker'
  },
  'storage-mapping.json': {},
  'tailscale/lxc.container.conf': 'f882e185540f35593edc9f67f0bec7a35046f03147c674f15ba3f8bedb95e307',
  'tailscale/root.squashfs': '0ebb8b138a251782a362a9e89e39f57e111d79791e69e35d8e2e457950934878',
  'tailscale/root.squashfs.docker-digest': '1b1ea05ceb7b67fc019a016c41055c01a331482c5df316943617959273c85af8',
  'tailscale/run.json': {
    '#spec': 'service-manifest-run@1',
    config: 'lxc.container.conf',
    name: 'tailscale',
    'root-volume': 'root.squashfs',
    runlevel: 'app',
    storage: {
      'docker--var-lib-tailscale': {
        persistence: 'permanent'
      },
      'lxc-overlay': {
        persistence: 'boot'
      }
    },
    type: 'lxc',
    volumes: []
  },
  'tailscale/src.json': {
    '#spec': 'service-manifest-src@1',
    args: {
      PV_LXC_EXTRA_CONF: 'lxc.cgroup.devices.allow = c 10:200 rwm',
      PV_RUNLEVEL: 'app'
    },
    config: {},
    docker_digest: 'registry.gitlab.com/highercomve/ph-tailscale@sha256:248490b097e37fb6552cfeb7db2a55823a5d6c632be892e998abbd61c6420460',
    docker_name: 'registry.gitlab.com/highercomve/ph-tailscale',
    docker_source: 'local,remote',
    docker_tag: 'arm32v6',
    persistence: {},
    template: 'builtin-lxc-docker'
  },
  'tailscale/v0alpha.app.manifest.json': {
    '#spec': 'app-manifest@1',
    _configfiles: [
      {
        file: '/etc/tailscale/config.json',
        name: 'tailscale.config',
        schema: {
          description: 'Configure your tailscale vpn',
          properties: {
            acceptdns: {
              default: true,
              title: 'Accept dns',
              type: 'boolean'
            },
            acceptroutes: {
              default: false,
              title: 'Accept routes',
              type: 'boolean'
            },
            advertiseroutes: {
              title: 'Advertise routes',
              type: 'string'
            },
            advertisetags: {
              title: 'Advertise tags',
              type: 'string'
            },
            hostname: {
              title: 'Hostname',
              type: 'string'
            },
            hostroutes: {
              default: true,
              title: 'Host routes',
              type: 'boolean'
            },
            key: {
              title: 'Authorization key',
              type: 'string'
            },
            loginserver: {
              title: 'Login server',
              type: 'string'
            },
            netfiltermode: {
              enum: [
                'on',
                'nodivert',
                'off'
              ],
              title: 'Netfilter mode',
              type: 'string'
            },
            shieldsup: {
              default: false,
              title: 'Shields up',
              type: 'boolean'
            },
            snatsubnetroutes: {
              default: false,
              title: 'Snat subnet routes',
              type: 'boolean'
            }
          },
          required: [
            'key'
          ],
          title: 'Tailscale configuration arguments',
          type: 'object'
        },
        type: 'jsonschema',
        ui: {
          acceptdns: {
            'ui:description': 'accept DNS configuration from the admin panel (default: true)'
          },
          acceptroutes: {
            'ui:description': 'accept routes advertised by other Tailscale nodes (default: false)'
          },
          advertiseroutes: {
            'ui:description': 'routes to advertise to other nodes (comma-separated, e.g. 10.0.0.0/8,192.168.0.0/24)'
          },
          advertisetags: {
            'ui:description': 'ACL tags to request (comma-separated, e.g. eng,montreal,ssh)'
          },
          hostname: {
            'ui:description': 'hostname to use instead of the one provided by the OS'
          },
          hostroutes: {
            'ui:description': 'install host routes to other Tailscale nodes'
          },
          key: {
            'ui:autofocus': true
          },
          loginserver: {
            'ui:description': 'base URL of control server (default: https://login.tailscale.com)'
          },
          netfiltermode: {
            'ui:description': 'netfilter mode (one of on, nodivert, off)'
          },
          shieldsup: {
            'ui:description': 'don\'t allow incoming connections (default: false)'
          },
          snatsubnetroutes: {
            'ui:description': 'source NAT traffic to local routes advertised with -advertise-routes (default: true)'
          },
          'ui:order': [
            'key',
            'hostname',
            '*'
          ]
        }
      }
    ],
    _metaconfig: [
      {
        'meta-key-prefix': 'tailscale',
        name: 'tailscale.meta',
        schema: {
          description: '',
          properties: {
            enabled: {
              default: false,
              title: 'Tailscale',
              type: 'boolean'
            }
          },
          required: [],
          title: '',
          type: 'object'
        },
        type: 'jsonschema',
        ui: {}
      }
    ],
    _readme: 'You can go to https://login.tailscale.com/admin/authkeys and get a new pre-auth key to claim you device on tailscale.',
    _status: [
      {
        properties: [
          {
            default: 'Not connected',
            key: 'interfaces.tailscale0.ipv4.0',
            title: 'VPN tunnel IP',
            type: 'device-meta'
          }
        ],
        title: 'Vpn connection'
      }
    ],
    description: 'Tailscale is a fast, modern, and secure VPN tunnel.',
    icon: 'vpn_key',
    logo: '',
    title: 'Tailscale VPN',
    version: '0.0.3'
  },
  'wireguard-vpn/lxc.container.conf': 'f044aa377587d294143e0666160c3adf0f9577d76e25c37573e9368f93787311',
  'wireguard-vpn/root.squashfs': '2ec01d4af438ae76e0fc56f8f84b52c4b0880eff88299acd42740187846761c1',
  'wireguard-vpn/root.squashfs.docker-digest': '0e2e7cb784707ef1e2b18b3b195293aade900affc1cbf398a726c07486586632',
  'wireguard-vpn/run.json': {
    '#spec': 'service-manifest-run@1',
    config: 'lxc.container.conf',
    name: 'wireguard-vpn',
    'root-volume': 'root.squashfs',
    runlevel: 'app',
    storage: {
      'docker--var-lib-wireguard': {
        persistence: 'permanent'
      },
      'lxc-overlay': {
        persistence: 'boot'
      }
    },
    type: 'lxc',
    volumes: []
  },
  'wireguard-vpn/src.json': {
    '#spec': 'service-manifest-src@1',
    args: {
      PV_RUNLEVEL: 'app'
    },
    config: {},
    docker_digest: 'registry.gitlab.com/pantacor/pv-platforms/ph-wireguard@sha256:bfecd137f0794f03357133eabc79368a936de7f258fcf57cac6991eec2261d0e',
    docker_name: 'registry.gitlab.com/pantacor/pv-platforms/ph-wireguard',
    docker_source: 'local,remote',
    docker_tag: 'arm32v6',
    persistence: {},
    template: 'builtin-lxc-docker'
  },
  'wireguard-vpn/v0alpha.app.manifest.json': {
    '#spec': 'app-manifest@1',
    _configfiles: [
      {
        file: '/etc/wireguard/config.json',
        name: 'wireguard.config',
        schema: {
          description: 'Configure your wireguard vpn',
          properties: {
            raw: {
              title: 'Raw configuration content',
              type: 'string'
            }
          },
          required: [
            'raw'
          ],
          title: 'Wireguard VPN',
          type: 'object'
        },
        type: 'jsonschema',
        ui: {
          raw: {
            'ui:autofocus': true,
            'ui:options': {
              rows: 20
            },
            'ui:widget': 'textarea'
          }
        }
      }
    ],
    _metaconfig: [
      {
        'meta-key-prefix': 'wireguard-vpn',
        name: 'wireguard.meta',
        schema: {
          description: '',
          properties: {
            enabled: {
              default: false,
              title: 'Wireguard VPN',
              type: 'boolean'
            }
          },
          required: [],
          title: '',
          type: 'object'
        },
        type: 'jsonschema',
        ui: {}
      }
    ],
    _status: [
      {
        properties: [
          {
            default: 'Not connected',
            key: 'interfaces.pantawg.ipv4.0',
            title: 'VPN tunnel IP',
            type: 'device-meta'
          }
        ],
        title: 'Vpn connection'
      }
    ],
    description: 'WireGuard is a fast, modern, and secure VPN tunnel.',
    icon: 'vpn_key',
    logo: '',
    title: 'WireGuard VPN',
    version: '0.0.5'
  }
}
